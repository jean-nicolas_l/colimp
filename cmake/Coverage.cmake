# Copyright (c) 2020, Jean-Nicolas Lang, jlang@physik.uzh.ch
# -----------------------------------------------------------------------------

set(LCOV_INDEX_FILE COVERAGE.info)
add_custom_command(OUTPUT ${LCOV_INDEX_FILE}
                   COMMAND
                   ${LCOV_EXECUTABLE}
                    --capture
                    --base-directory ${CMAKE_CURRENT_BINARY_DIR}
                    --directory ${CMAKE_CURRENT_BINARY_DIR}
                    --output-file ${LCOV_INDEX_FILE}
                   COMMAND
                   ${LCOV_EXECUTABLE}
                    --remove ${LCOV_INDEX_FILE}
                    '*/tests/*' '/usr/*' '*/util/*'
                    --output-file ${LCOV_INDEX_FILE}
                   COMMAND
                   ${LCOV_EXECUTABLE} --list ${LCOV_INDEX_FILE} > "COVERAGE_SUMMARY.info"
                   WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                   DEPENDS
                   check
                   COMMENT "Generating coverage with lcov")

add_custom_target(Lcov ALL DEPENDS ${LCOV_INDEX_FILE})

add_custom_target(Lcov_force
                  COMMAND
                   ${LCOV_EXECUTABLE}
                    --capture
                    --base-directory ${CMAKE_CURRENT_BINARY_DIR}
                    --directory ${CMAKE_CURRENT_BINARY_DIR}
                    --output-file ${LCOV_INDEX_FILE}
                   COMMAND
                   ${LCOV_EXECUTABLE}
                    --remove ${LCOV_INDEX_FILE}
                    '*/tests/*' '/usr/*' '*/util/*'
                    --output-file ${LCOV_INDEX_FILE}
                   COMMAND
                   ${LCOV_EXECUTABLE} --list ${LCOV_INDEX_FILE} > "COVERAGE_SUMMARY.info"
                   WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
set_target_properties(Lcov_force PROPERTIES EXCLUDE_FROM_ALL TRUE)
if (GENHTML_EXECUTABLE)
  set(LCOV_HTML_INDEX_FILE CODE_COVERAGE/index.html)
  add_custom_command(OUTPUT ${LCOV_HTML_INDEX_FILE}
                     COMMAND
                     ${GENHTML_EXECUTABLE} ${LCOV_INDEX_FILE} --output-directory "CODE_COVERAGE"
                     WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                     DEPENDS
                     ${LCOV_INDEX_FILE}
                     COMMENT "Generating coverage with lcov")
   add_custom_target(Lcov_html ALL DEPENDS ${LCOV_HTML_INDEX_FILE})

  add_custom_command(OUTPUT ${LCOV_HTML_INDEX_FILE}
                     COMMAND
                     ${GENHTML_EXECUTABLE} ${LCOV_INDEX_FILE} --output-directory "CODE_COVERAGE"
                     WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                     DEPENDS
                     ${LCOV_INDEX_FILE}
                     COMMENT "Generating coverage with lcov")
  add_custom_target(Lcov_html_force
                    COMMAND ${GENHTML_EXECUTABLE} ${LCOV_INDEX_FILE} --output-directory "CODE_COVERAGE"
                    DEPENDS Lcov_force
                    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                    COMMENT "Generating coverage with lcov")
  set_target_properties(Lcov_html_force PROPERTIES EXCLUDE_FROM_ALL TRUE)
endif()
