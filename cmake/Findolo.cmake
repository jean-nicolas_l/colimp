# Copyright (c) 2020, Jean-Nicolas Lang, jlang@physik.uzh.ch
# -----------------------------------------------------------------------------

include(FindPackageHandleStandardArgs)

if(OLO_INCLUDE_DIR AND OLO_LIBRARY)
  set(OLO_FIND_QUIETLY TRUE)
endif()

find_path(OLO_INCLUDE_DIR NAMES avh_olo.mod)
find_library(OLO_LIBRARY NAMES avh_olo)

find_package_handle_standard_args(olo DEFAULT_MSG OLO_LIBRARY OLO_INCLUDE_DIR)

if(OLO_FOUND)
  message(STATUS "Found 'olo': ${OLO_LIBRARY}.")
  include_directories(${OLO_INCLUDE_DIR})
endif()
