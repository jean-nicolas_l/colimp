module coliflower
    use params_coli_sp, only: sp=>prec
    use params_coli_dp, only: dp=>prec
    use params_coli_lp, only: lp=>prec
    use params_coli_qp, only: qp=>prec
#import "colisets.inc"
#import "a0.inc"
#import "b0.inc"
#import "c0.inc"
#import "d0.inc"

    implicit none
    public initcoli

    public setmuuv2,setmuir2
    public getmuuv2,getmuir2
    public setdeltauv,setdeltair
    public getdeltauv,getdeltair
    public setminf2
#import "colisets.proc"

    public A0
#import "a0.proc"

    public B0,DB0,B1,DB1,B00,B11
#import "b0.proc"

    public C0
#import "c0.proc"

    public D0
#import "d0.proc"

contains

    subroutine initcoli() &
        bind(C,name='init_coliflower')
      use, intrinsic :: iso_c_binding
#import "coliinit.inc"
      implicit none
#import "coliinit.proc"
    end subroutine initcoli


#import "colisets_C_iface.proc"
#import "a0_C_iface.proc"
#import "b0_C_iface.proc"
#import "c0_C_iface.proc"
#import "d0_C_iface.proc"

end module coliflower
