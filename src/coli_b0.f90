!***********************************************************************
!                                                                      *
!     Scalar 2-point functions                                         *
!                                                                      *
!***********************************************************************
!                                                                      *
!     last changed  15.06.11  Ansgar Denner                            *
!     errorflags    22.05.13  Ansgar Denner                            *
!     dreal in fpve_coli calls 15.11.13 AD                             *
!     DBn_coli  18.06.14                                               *
!     DBn_coli,DB00_coli,   02.04.15                                   *
!                                                                      *
!***********************************************************************
! subroutines:                                                         *
! crootse,elminf2iv_coli,errB0_coli                                *
! functions:                                                           *
! B0_coli, B1_coli, B11_coli, B00_coli , B111_coli, B001_coli          *
! BB0_coli, DB0_coli, DB1_coli, DB00_coli                              *
! Bn_coli,B00n_coli, B0000n_coli                                       *
! DBn_coli                                                             *
! fpve_coli, yfpve_coli, xyfpve_coli, xlogxe_coli                      *
#import "prectemplate.inc"

module coli_b0_<T>
  use params_coli_<T>
  use common_coli_<T>
  use checkparams_coli
  use coli_aux_<T>, only: elimminf2_coli,cln_coli,eta2_coli
  implicit none

  contains

!***********************************************************************
  subroutine crootse_coli(p2,m12,m22,x1,x2,y1,y2,r)
!***********************************************************************
!     roots of quadratic equation                                      *
!     p2*x*x+(m22-m12-p2)*x+m12     = p2*(x-x1)*(x-x2)                 *
!                                     p2*(x-1+y1)*(x-1+y2)             *
!----------------------------------------------------------------------*
!     21.07.93 Ansgar Denner      last changed  21.10.08 ad            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: p2,m12,m22
  complex(kind=prec) :: x1,x2,y1,y2,r
  complex(kind=prec) :: q12,q22,m1,m2


!--->   case y1=y2=0  <-> B0_coli(m2,m,0) makes it difficult!
  m1 = sqrt(m12)
  m2 = sqrt(m22)
  r=sqrt((p2-(m1+m2)**2)*(p2-(m1-m2)**2))
!     r=sqrt(p2*p2+m12*m12+m22*m22-rtwo*(m12*m22+m12*p2+m22*p2))
  q12=m12-m22+p2
  q22=m22-m12+p2
  x1=(q12+r)/(rtwo*p2)
  x2=(q12-r)/(rtwo*p2)


  if (abs(x2).gt.abs(x1)) then
    x1=m12/(p2*x2)
  elseif (abs(x1).gt.abs(x2)) then
    x2=m12/(p2*x1)
  endif
  y2=(q22+r)/(rtwo*p2)
  y1=(q22-r)/(rtwo*p2)
  if (abs(y2).gt.abs(y1)) then
    y1=m22/(p2*y2)
  elseif (abs(y1).gt.abs(y2)) then
    y2=m22/(p2*y1)
  endif


  end

!***********************************************************************
  function B0_coli(p2,m12,m22)
!***********************************************************************
!     scalar one loop integral B0_coli                                 *
!     p2 outer momentum, m12,m22 inner masses squared                  *
!----------------------------------------------------------------------*
!     19.08.93 Ansgar Denner      last changed 24.10.08 ad             *
!***********************************************************************
  implicit none
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  complex(kind=prec) :: B0_coli
  complex(kind=prec) :: x1,x2,y1,y2,r
  logical :: finarg,nocalc
  logical :: errorwriteflag

  call elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)
  nocalc=.true.

! ---> special cases for small masses
  if (.not.finarg) then
    nocalc=.false.
    if (q2.eq.mm12.and.mm22.eq.rnul.and.q2.ne.rnul) then
      B0_coli = rtwo - log(mm12*coliminfscale2/muuv2)
    else if (q2.eq.mm22.and.mm12.eq.rnul.and.q2.ne.rnul) then
      B0_coli = rtwo - log(mm22*coliminfscale2/muuv2)
    else if (q2.eq.rnul.and.mm12.eq.mm22.and.mm12.ne.rnul) then
      B0_coli = -log(mm12*coliminfscale2/muuv2)
    else if (q2.eq.rnul.and.mm12.eq.rnul.and.mm22.ne.rnul) then
      B0_coli = rone - log(mm22*coliminfscale2/muuv2)
    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.ne.rnul) then
      B0_coli = rone - log(mm12*coliminfscale2/muuv2)
    else if (q2.ne.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      B0_coli = rtwo - cln_coli(-q2*coliminfscale2/muuv2,-rone)
    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      B0_coli = log(muuv2/muir2)
#ifdef SING
      B0_coli = B0_coli-delta1ir

#endif
    else
      nocalc=.true.
    endif
  endif

!---->  finite argument case
  if (nocalc) then
    if (abs(q2).gt.calacc*abs(mm12+mm22)) then
      call crootse_coli(q2,mm12,mm22,x1,x2,y1,y2,r)
      if (abs(y1).gt.rhlf.and.abs(y2).gt.rhlf) then
        B0_coli= (-log(mm22/muuv2) - fpve_coli(1,x1,y1,rone) &
                               - fpve_coli(1,x2,y2,-rone))

      elseif (abs(x1).lt.10*rone.and.abs(x2).lt.10*rone) then
        B0_coli = rtwo - cln_coli(q2/muuv2,-rone) &
             + xlogxe_coli(-x1,-rone) + xlogxe_coli(-x2,rone) &
             - xlogxe_coli(y1,-rone)  - xlogxe_coli(y2,rone)
        if (mm12.ne.rnul) B0_coli = B0_coli &
             - eta2_coli(-x1,-x2,mm12/q2,-rone,rone,-real(q2))


      elseif (abs(x1).gt.rhlf.and.abs(x2).gt.rhlf) then
        B0_coli= (-log(mm12/muuv2) - fpve_coli(1,y1,x1,-rone) &
                               - fpve_coli(1,y2,x2,rone))


      else
        call errB0_coli(q2,mm12,mm22,B0_coli,'B0_coli   ',12)
      endif
!---->  zero momentum
    elseif (abs(mm12-mm22).gt.calacc*abs(mm12+mm22)) then
      x2 = mm12/(mm12-mm22)
      y2 = mm22/(mm22-mm12)
      if (abs(y2).gt.rhlf) then
        B0_coli = (-log(mm22/muuv2) &
                   - fpve_coli(1,x2,y2,real(mm22-mm12)))


      else
        B0_coli = (-log(mm12/muuv2) &
                   - fpve_coli(1,y2,x2,real(mm12-mm22)))


      endif
    else if (mm22.ne.rnul) then


      B0_coli = -log(mm22/muuv2) + q2/(6*mm22)
    else
      write (*,*) 'error'
      call errB0_coli(q2,mm12,mm22,B0_coli,'B0_coli   ',20)
    endif
  endif


#ifdef SING
  B0_coli = B0_coli + deltauv
#endif


  end

!***********************************************************************
  function BB0_coli(p2,m12,m22)
!***********************************************************************
!     scalar one loop integral (B0_coli(q2)-B0_coli(0))/q2             *
!     p2 outer momentum, m12,m22 inner masses squared                  *
!     appears only multiplied with (m12-m22)                           *
!----------------------------------------------------------------------*
!     19.08.93 Ansgar Denner      last changed 21.10.08 ad             *
!***********************************************************************
  implicit none
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  complex(kind=prec) :: BB0_coli
  complex(kind=prec) :: x1,x2,y1,y2,r
  logical :: finarg
  logical :: errorwriteflag

  call elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)

  if(mm12.ne.cnul.or.mm22.ne.cnul)then
    if (finarg) then
!---->  general case
      if (abs(q2).gt.calacc*abs(mm12+mm22)) then
        call crootse_coli(q2,mm12,mm22,x1,x2,y1,y2,r)


        if (abs(x1+y2).lt.calacc*abs(x1+x2).or. &
            abs(x2+y1).lt.calacc*abs(x1+x2)) then
          if(m12*m22.ne.rnul) then
            BB0_coli = rone + (m12*cln_coli(-p2/m12,-rone) &
                - m22*cln_coli(-p2/m22,-rone))/(m22-m12)
          else if (m12.ne.rnul) then
            BB0_coli = rone - cln_coli(-p2/m12,-rone)
          else if (m22.ne.rnul) then
            BB0_coli = rone - cln_coli(-p2/m22,-rone)
          else
            call errB0_coli(q2,m12,m22,BB0_coli,'BB0_coli  ',24)
            write(nerrout_coli,*) 'BB0 ', &
                p2,m12,m22,q2,mm12,mm22,finarg
          endif
        else if (abs(x1).lt.calacc*abs(x1+x2)) then
          BB0_coli = -fpve_coli(1,x2,y2,-rone)/q2


        else if (abs(x2).lt.calacc*abs(x1+x2)) then
          BB0_coli = -fpve_coli(1,x1,y1,rone)/q2


        else if (abs(y2).lt.calacc*abs(y1+y2)) then
          BB0_coli = -fpve_coli(1,y1,x1,-rone)/q2


        else if (abs(y1).lt.calacc*abs(y1+y2)) then
          BB0_coli = -fpve_coli(1,y2,x2,rone)/q2


!:  mm22-mm12 = q2*(1-x1-x2)
        else if (abs(y1-x2).gt.sqrt(calacc)*abs(y1+x2).or. &
              abs(y2-x1).gt.sqrt(calacc)*abs(y2+x1)) then
          if(abs(x1).gt.10*rone) then
            BB0_coli = -rhlf/x1 - yfpve_coli(2,x1,y1,rone)/x1


          else
            BB0_coli = -rhlf - yfpve_coli(1,x1,y1,rone)


          endif
          if(abs(x2).gt.10*rone) then
            BB0_coli = BB0_coli - rhlf/x2 &
                - yfpve_coli(2,x2,y2,-rone)/x2


          else
            BB0_coli = BB0_coli - rhlf - yfpve_coli(1,x2,y2,-rone)


          endif
          BB0_coli = BB0_coli/(mm22-mm12)
        else
          if (abs(x1).gt.10*rone) then
            BB0_coli = (rhlf + (rone-rtwo*x1)*fpve_coli(2,x1,y1,rone)) &
                /(x1*x1*q2)


          elseif (y1.ne.rnul.and.x1.ne.rnul) then
            BB0_coli = (rtwo + (rone-rtwo*x1)*fpve_coli(0,x1,y1,rone)) &
                /q2


          else
            call errB0_coli(q2,mm12,mm22,BB0_coli,'BB0_coli  ',16)
          endif
        endif
!---- >  zero momentum
      elseif (abs(mm12-mm22).gt.calacc*abs(mm12+mm22)) then
        x2 = mm12/(mm12-mm22)
        y2 = mm22/(mm22-mm12)
        if (abs(x2).lt.10*rone) then
          BB0_coli = (rhlf &
                      + yfpve_coli(1,x2,y2,real(mm22-mm12))) &
              /(mm12-mm22)
        else
          BB0_coli = (y2*fpve_coli(2,x2,y2,real(mm22-mm12)) &
                      + rhlf)/mm12
        endif
      else if (mm12.ne.rnul) then
        BB0_coli = rone/6/mm12
      else
        call errB0_coli(q2,mm12,mm22,BB0_coli,'BB0_coli  ',20)
      endif
    else
      if (q2.eq.mm12.and.mm22.eq.rnul.and.q2.ne.rnul) then
        BB0_coli = rone/q2
      else if (q2.eq.mm22.and.mm12.eq.rnul.and.q2.ne.rnul) then
        BB0_coli = rone/q2
      else if (q2.eq.rnul.and.mm12.eq.mm22.and.mm12.ne.rnul) then
        BB0_coli = rone/(6*mm12)
      else if (q2.eq.rnul.and.mm12.eq.rnul.and.mm22.ne.rnul) then
        BB0_coli = rone/(rtwo*mm22)
      else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.ne.rnul) then
        BB0_coli = rone/(rtwo*mm12)
      else
        call errB0_coli(q2,mm12,mm22,BB0_coli,'BB0_coli  ',28)
      endif
    endif
  else
    if(m12*m22.ne.rnul) then
      BB0_coli = (rone + (m12*cln_coli(-p2/(m12*coliminfscale2),-rone) &
          - m22*cln_coli(-p2/(m22*coliminfscale2),-rone)) &
          /(m22-m12))/p2
    else if (m12.ne.rnul) then
      BB0_coli = (rone - cln_coli(-p2/(m12*coliminfscale2),-rone))/p2
    else if (m22.ne.rnul) then
      BB0_coli = (rone - cln_coli(-p2/(m22*coliminfscale2),-rone))/p2
    else


      call errB0_coli(q2,m12,m22,BB0_coli,'BB0_coli  ',24)
    endif
#ifdef SING
      BB0_coli = BB0_coli + delta1ir
#endif
  endif



  end

!***********************************************************************
  function DB0_coli(p2,m12,m22)
!***********************************************************************
!     derivative of scalar one loop integral B0_coli                   *
!     p2 outer momentum, m12,m22 inner masses squared                  *
!----------------------------------------------------------------------*
!     19.08.93 Ansgar Denner      last changed  28.10.08 ad            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  complex(kind=prec) :: DB0_coli
  complex(kind=prec) :: x1,x2,y1,y2,r
  logical :: finarg,nocalc
  logical :: errorwriteflag

#ifdef WARN
  logical flag
  data    flag /.true./
  save    flag
#endif

  call elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)
  nocalc=.true.

! ----> special cases for small masses
  if (.not.finarg) then
    nocalc=.false.
    if (q2.eq.mm12.and.m22.eq.rnul.and.q2.ne.rnul) then
      DB0_coli = -(rone + rhlf*log(muir2/(mm12*coliminfscale2)))/q2
#ifdef SING
      DB0_coli = DB0_coli -rhlf*delta1ir/q2
#endif
    else if (q2.eq.mm22.and.m12.eq.rnul.and.q2.ne.rnul) then
      DB0_coli = -(rone + rhlf*log(muir2/(mm22*coliminfscale2)))/q2
#ifdef SING
      DB0_coli = DB0_coli -rhlf*delta1ir/q2
#endif
    else if (q2.eq.rnul.and.mm12.eq.mm22.and.mm12.ne.rnul) then
      DB0_coli = rone/(6*mm12)
    else if (q2.eq.rnul.and.mm12.eq.rnul.and.mm22.ne.rnul) then
      DB0_coli = rone/(rtwo*mm22)
    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.ne.rnul) then
      DB0_coli = rone/(rtwo*mm12)
    else if (q2.ne.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      DB0_coli = -rone/(q2)
    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      DB0_coli = cnul
    else
      nocalc=.true.
    endif
  endif

!---->  finite argument case
  if (nocalc) then
! IR-singular case
    if(p2.eq.m12+m22.and.m12*m22.eq.rnul) then
      DB0_coli = -(rone + rhlf*log(muir2/(m12+m22)))/p2
#ifdef SING
      DB0_coli = DB0_coli -rhlf*delta1ir/p2
#endif

!---->  general case
    else if (abs(q2).gt.calacc*abs(mm12+mm22)) then
      call crootse_coli(q2,mm12,mm22,x1,x2,y1,y2,r)


!:  r = q2*(x1-x2)
      if (abs(x1-x2).gt.sqrt(calacc)*abs(x1+x2)) then


        if (abs(x1).gt.rone/10.or.abs(x2).gt.rone/10) then
          DB0_coli = (yfpve_coli(1,x2,y2,-rone) &
              - yfpve_coli(1,x1,y1,rone))/r


        else
          DB0_coli = (xyfpve_coli(0,x2,y2,-rone) &
              - xyfpve_coli(0,x1,y1,rone))/r &
                   -rone/q2


        endif


      else if (abs(x1-x2).gt.calacc*abs(x1+x2)) then


        DB0_coli = (yfpve_coli(1,x2,y2,-rone) &
            - yfpve_coli(1,x1,y1,rone))/r

      elseif (abs(x1).gt.10*rone) then
        DB0_coli = -(rhlf + (rone-rtwo*x1)*fpve_coli(2,x1,y1,rone)) &
            /(q2*x1*x1)


      elseif (abs(y1).gt.calacc.and.abs(x1).gt.calacc) then
        DB0_coli = -( rtwo + (rone-rtwo*x1)*fpve_coli(0,x1,y1,rone) )/q2



      elseif (mm12.eq.rnul.and.q2.eq.mm22) then
        DB0_coli = -(rone + rhlf*log(m12/mm22))/q2

      elseif (mm22.eq.rnul.and.q2.eq.mm12) then
        DB0_coli = -(rone + rhlf*log(m22/mm12))/q2


      else
        call errB0_coli(q2,mm12,mm22,DB0_coli,'DB0_coli  ',12)
      endif
!---->  zero momentum
    elseif (abs(mm12-mm22).gt.calacc*abs(mm12+mm22)) then


      x2 = mm12/(mm12-mm22)
      y2 = mm22/(mm22-mm12)
      if (abs(x2).lt.10*rone) then
        DB0_coli = (rhlf + yfpve_coli(1,x2,y2,real(mm22-mm12))) &
            /(mm12-mm22)


      else
        DB0_coli = (y2*fpve_coli(2,x2,y2,real(mm22-mm12)) &
                      + rhlf)/mm12


      endif
    else if (mm12.ne.rnul) then
      DB0_coli = rone/(6*mm12)


    else
      call errB0_coli(q2,mm12,mm22,DB0_coli,'DB0_coli  ',20)
    endif
  endif


  end

!***********************************************************************
  function B1_coli(p2,m12,m22)
!***********************************************************************
!     vector one loop integral B1_coli                                 *
!     p2 outer momentum, m12,m22 inner masses squared                  *
!----------------------------------------------------------------------*
!     19.08.93 Ansgar Denner      last changed  24.10.08 ad            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  logical :: finarg,nocalc
  complex(kind=prec) :: B1_coli
  complex(kind=prec) :: x1,x2,y1,y2,r
  logical :: errorwriteflag

  call elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)
  nocalc=.true.

! ----> special cases for small masses
  if (.not.finarg) then
    nocalc=.false.
    if (q2.eq.mm12.and.mm22.eq.rnul.and.q2.ne.rnul) then
      B1_coli = -1.5_prec + rhlf*log(mm12*coliminfscale2/muuv2)
    else if (q2.eq.mm22.and.mm12.eq.rnul.and.q2.ne.rnul) then
      B1_coli = -rhlf + rhlf*log(mm22*coliminfscale2/muuv2)
    else if (q2.eq.rnul.and.mm12.eq.mm22.and.mm12.ne.rnul) then
      B1_coli = rhlf*log(mm12*coliminfscale2/muuv2)
    else if (q2.eq.rnul.and.mm12.eq.rnul.and.mm22.ne.rnul) then
      B1_coli = -0.25_prec + rhlf*log(mm22*coliminfscale2/muuv2)
    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.ne.rnul) then
      B1_coli = -0.75_prec + rhlf*log(mm12*coliminfscale2/muuv2)
    else if (q2.ne.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      B1_coli = -rone + rhlf*cln_coli(-q2*coliminfscale2/muuv2,-rone)
    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      B1_coli = -rhlf*log(muuv2/muir2)
#ifdef SING
      B1_coli = B1_coli + rhlf*delta1ir
#endif
    else
      nocalc=.true.
    endif
  endif


!---->  finite argument case
  if(nocalc) then
    if (abs(q2).gt.calacc*abs(mm12+mm22)) then
      call crootse_coli(q2,mm12,mm22,x1,x2,y1,y2,r)

      if (abs(y1).gt.rhlf.and.abs(y2).gt.rhlf) then
        B1_coli = -(-log(mm22/muuv2) &
             - fpve_coli(2,x1,y1,rone) - fpve_coli(2,x2,y2,-rone))/rtwo
      elseif (abs(x1).lt.10*rone.and.abs(x2).lt.10*rone) then
        B1_coli = rone - cln_coli(q2/muuv2,-rone) &
             + x1*xlogxe_coli(-x1,-rone) + x1 &
            + x2*xlogxe_coli(-x2,rone) + x2 &
             - (rone+x1)*xlogxe_coli(y1,-rone) &
            - (rone+x2)*xlogxe_coli(y2,rone)
        if (mm12.ne.rnul) B1_coli = B1_coli &
             - eta2_coli(-x1,-x2,mm12/q2,-rone,rone,-real(q2))
        B1_coli = -B1_coli/rtwo
      elseif (abs(x1).gt.rhlf.and.abs(x2).gt.rhlf) then
        if (abs(x1).gt.10) then
          B1_coli = ( log(mm12/muuv2) &
              + fpve_coli(2,x1,y1,rone) - fpve_coli(0,x1,y1,rone) &
              + ((rone+x2)*fpve_coli(1,y2,x2,rone) + rhlf)  )/rtwo
        else
          B1_coli = ( log(mm12/muuv2) &
              + ((rone+x1)*fpve_coli(1,y1,x1,-rone) + rhlf) &
              + fpve_coli(2,x2,y2,-rone) - fpve_coli(0,x2,y2,-rone)) &
                 /rtwo
        endif
      else
        call errB0_coli(q2,mm12,mm22,B1_coli,'B1_coli   ',12)
      endif
!---->  zero momentum
    elseif (abs(mm12-mm22).gt.calacc*abs(mm12+mm22)) then
      x2 = mm12/(mm12-mm22)
      y2 = mm22/(mm22-mm12)
      if(abs(y2).gt.rhlf) then
        B1_coli = -(-log(mm22/muuv2) &
            - fpve_coli(2,x2,y2,real(mm22-mm12)))/rtwo
      else
        B1_coli = -(-log(mm12/muuv2) &
            - (1+x2)*fpve_coli(1,y2,x2,real(mm12-mm22)) &
             - rhlf)/rtwo
      endif
    else if (mm22.ne.rnul) then
      B1_coli = -(-log(mm22/muuv2))/rtwo
    else
      call errB0_coli(q2,mm12,mm22,B1_coli,'B1_coli   ',20)
    endif
  endif


#ifdef SING
  B1_coli = B1_coli - rhlf * deltauv
#endif
  end

!***********************************************************************
  function DB1_coli(p2,m12,m22)
!***********************************************************************
!     derivative of vector one loop integral B1_coli                   *
!     p2 outer momentum, m12,m22 inner masses squared                  *
!----------------------------------------------------------------------*
!     19.08.93 Ansgar Denner      last changed  28.10.08 ad            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  complex(kind=prec) :: DB1_coli
  complex(kind=prec) :: x1,x2,y1,y2,r
  logical :: finarg,nocalc
  logical :: errorwriteflag

#ifdef WARN
  logical flag
  data    flag /.true./
  save    flag
#endif

  call elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)
  nocalc=.true.

  if (.not.finarg) then
    nocalc=.false.
    if (q2.eq.mm12.and.m22.eq.rnul.and.q2.ne.rnul) then
      DB1_coli = (3 + log(muir2/(mm12*coliminfscale2)))/(rtwo*q2)
#ifdef SING
      DB1_coli = DB1_coli + delta1ir/(rtwo*p2)
#endif
    else if (q2.eq.mm22.and.mm12.eq.rnul.and.q2.ne.rnul) then
      DB1_coli = -rhlf/q2
    else if (q2.eq.rnul.and.mm12.eq.mm22.and.mm12.ne.rnul) then
      DB1_coli = -rone/(12*mm12)
    else if (q2.eq.rnul.and.mm12.eq.rnul.and.mm22.ne.rnul) then
      DB1_coli = -rone/(6*mm22)
    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.ne.rnul) then
      DB1_coli = -rone/(3*mm12)
    else if (q2.ne.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      DB1_coli = rone/(rtwo*q2)
    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      DB1_coli = cnul
    else
      nocalc=.true.
    endif

  endif

!---->  finite argument case
  if (nocalc) then
! IR-singular case
    if(p2.eq.m12.and.m22.eq.rnul) then
      DB1_coli = (3 + log(muir2/p2))/(rtwo*p2)
#ifdef SING
      DB1_coli = DB1_coli + delta1ir/(rtwo*p2)
#endif
!---->  general case
    else if (abs(q2).gt.calacc*abs(mm12+mm22)) then
      call crootse_coli(q2,mm12,mm22,x1,x2,y1,y2,r)


      if (abs(x1-x2).gt.sqrt(calacc)*abs(x1+x2)) then


        if (abs(x1).gt.rone/10.or.abs(x2).gt.rone/10) then
          DB1_coli = -(yfpve_coli(2,x2,y2,-rone) &
              - yfpve_coli(2,x1,y1,rone))/r


        else
          DB1_coli = -(x2*xyfpve_coli(0,x2,y2,-rone) &
              -x1*xyfpve_coli(0,x1,y1,rone)) &
               /r    +(x1+x2-rhlf)/q2


        endif


      else if (abs(x1-x2).gt.calacc*abs(x1+x2)) then


        DB1_coli = -(yfpve_coli(2,x2,y2,-rone) &
            - yfpve_coli(2,x1,y1,rone))/r

      elseif (abs(x1).gt.10*rone) then
        DB1_coli = (rtwo/3 + (rtwo-3*x1)*fpve_coli(3,x1,y1,rone)) &
                /(q2*x1*x1)

      elseif (abs(y1).gt.calacc) then
        DB1_coli = ( 3/rtwo + (rtwo-3*x1)*fpve_coli(1,x1,y1,rone) ) &
            /q2


      elseif (mm22.eq.rnul.and.q2.eq.mm12) then
        DB1_coli = (3 + log(m22/q2))/(q2*rtwo)
      else
        call errB0_coli(q2,mm12,mm22,DB1_coli,'DB1_coli  ',12)
      endif
!---->  zero momentum
    elseif (abs(mm12-mm22).gt.calacc*abs(mm12+mm22)) then
      x2 = mm12/(mm12-mm22)
      y2 = mm22/(mm22-mm12)
      if (abs(x2).lt.10*rone) then
        DB1_coli = -(rone/3 + yfpve_coli(2,x2,y2,real(mm22-mm12))) &
            /(mm12-mm22)
      else
        DB1_coli = -(y2*fpve_coli(3,x2,y2,real(mm22-mm12)) &
                    + rone/3)/mm12
      endif
    else if (mm12.ne.rnul) then
      DB1_coli  = -rone/(12*mm12)
    else
      call errB0_coli(q2,mm12,mm22,DB1_coli,'DB1_coli  ',20)
    endif
  endif

  end

!***********************************************************************
  function B11_coli(p2,m12,m22)
!***********************************************************************
!     tensor one loop integral B11_coli                                *
!     p2 outer momentum, m1,m2 inner masses squared                    *
!----------------------------------------------------------------------*
!     19.08.93 Ansgar Denner      last changed  24.10.08 ad            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  complex(kind=prec) :: B11_coli
  complex(kind=prec) :: x1,x2,y1,y2,r
  logical :: finarg,nocalc
  logical :: errorwriteflag


  call elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)
  nocalc=.true.

! ---> special cases for small masses
  if (.not.finarg) then
    nocalc=.false.
    if (q2.eq.mm12.and.mm22.eq.rnul.and.q2.ne.rnul) then
      B11_coli = 11._prec/9 - log(mm12*coliminfscale2/muuv2)/3
    else if (q2.eq.mm22.and.mm12.eq.rnul.and.q2.ne.rnul) then
      B11_coli = rtwo/9 - log(mm22*coliminfscale2/muuv2)/3
    else if (q2.eq.rnul.and.mm12.eq.mm22.and.mm12.ne.rnul) then
      B11_coli = -log(mm12*coliminfscale2/muuv2)/3
    else if (q2.eq.rnul.and.mm12.eq.rnul.and.mm22.ne.rnul) then
      B11_coli = rone/9 - log(mm22*coliminfscale2/muuv2)/3
    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.ne.rnul) then
      B11_coli = 11._prec/18 - log(mm12*coliminfscale2/muuv2)/3
    else if (q2.ne.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      B11_coli = 13._prec/18 - cln_coli(-q2*coliminfscale2/muuv2,-rone) &
          /3
    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      B11_coli = log(muuv2/muir2)/3
#ifdef SING
      B11_coli = B11_coli - delta1ir/3
#endif
    else
      nocalc=.true.
    endif
  endif

!---->  finite argument case
  if (nocalc) then
!---->  general case
    if (abs(q2).gt.calacc*abs(mm12+mm22)) then
      call crootse_coli(q2,mm12,mm22,x1,x2,y1,y2,r)
      if (abs(y1).gt.rhlf.and.abs(y2).gt.rhlf) then
        B11_coli = (-log(mm22/muuv2) &
             - fpve_coli(3,x1,y1,rone) - fpve_coli(3,x2,y2,-rone))/3
      elseif (abs(x1).lt.10*rone.and.abs(x2).lt.10*rone) then
        B11_coli = rtwo/3 - cln_coli(q2/muuv2,-rone) &
             + x1**2*xlogxe_coli(-x1,-rone) + x1*x1 + x1/rtwo &
             + x2**2*xlogxe_coli(-x2, rone) + x2*x2 + x2/rtwo &
             - (rone+x1+x1*x1)*xlogxe_coli(y1,-rone) &
             - (rone+x2+x2*x2)*xlogxe_coli(y2,rone)
        if (mm12.ne.rnul) B11_coli = B11_coli &
             - eta2_coli(-x1,-x2,mm12/q2,-rone,rone,-real(q2))
        B11_coli = B11_coli/3
      elseif (abs(x1).gt.rhlf.and.abs(x2).gt.rhlf) then
        if (abs(x1).gt.10) then
          B11_coli = ( -log(mm12/muuv2) &
                - fpve_coli(3,x1,y1,rone) + fpve_coli(0,x1,y1,rone) &
              - ((x2*x2+x2+rone)*fpve_coli(1,y2,x2,rone) &
                    + x2/rtwo + rtwo/3) &
              )/3
        else
          B11_coli = ( -log(mm12/muuv2) &
              - ((x1*x1+x1+rone)*fpve_coli(1,y1,x1,-rone) &
                    + x1/rtwo + rtwo/3) &
                + fpve_coli(3,x2,y2,-rone) - fpve_coli(0,x2,y2,-rone) &
              )/3
        endif
      else
        call errB0_coli(q2,mm12,mm22,B11_coli,'B11_coli  ',12)
      endif
!---->  zero momentum
    elseif (abs(mm12-mm22).gt.calacc*abs(mm12+mm22)) then
      x2 = mm12/(mm12-mm22)
      y2 = mm22/(mm22-mm12)
      if(abs(y2).gt.rhlf) then
        B11_coli = (-log(mm22/muuv2) &
                    - fpve_coli(3,x2,y2,real(mm22-mm12))) &
            /3
      else
        B11_coli = (-log(mm12/muuv2) &
             + (rone+x2+x2*x2)*yfpve_coli(0,x2,y2,real(mm22-mm12)) &
             + x2*x2 + x2/rtwo + rone/3)/3
      endif
    else if(mm22.ne.rnul) then
      B11_coli = -log(mm22/muuv2)/3
    else
      call errB0_coli(q2,mm12,mm22,B11_coli,'B11_coli  ',20)
    endif
  endif
#ifdef SING
  B11_coli = B11_coli + deltauv/3
#endif
  end

!***********************************************************************
  function B00_coli(p2,m12,m22)
!***********************************************************************
!     one loop integral B00_coli                                       *
!     p2 outer momentum                                                *
!     m12,m22 inner masses squared                                     *
!----------------------------------------------------------------------*
!     19.08.93 Ansgar Denner      last changed  21.10.08 ad            *
!***********************************************************************
  use coli_a0_<T>, only: A0_coli=>A0_<T>
  implicit none
  complex(kind=prec) :: B00_coli
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  logical :: finarg
  logical :: errorwriteflag

  call elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)

  if (finarg) then
    if (abs(q2)/(abs(q2)+abs(mm12+mm22)).gt.calacc) then
      B00_coli = (q2+mm12-mm22)*B1_coli(q2,mm12,mm22) &
          + rtwo*mm12*B0_coli(q2,mm12,mm22) &
          + A0_coli(mm22) + mm12+mm22-q2/3
      B00_coli = B00_coli/6


    else
      B00_coli = (rtwo*(mm22*B0_coli(cnul,mm12,mm22) &
          + A0_coli(mm12)) + mm12 + mm22)/8
    endif
  else
    B00_coli = rnul
  endif
  end

!***********************************************************************
  function DB00_coli(p2,m12,m22)
!***********************************************************************
!     derivative of scalar one loop integral B20 = B00_coli            *
!     p2 outer momentum                                                *
!     m12,m22 inner masses squared                                     *
!     NOT REALLY TESTED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*
!----------------------------------------------------------------------*
!     17.10.05 Ansgar Denner      last changed  21.10.08 ad            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: DB00_coli
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  logical :: finarg
  logical :: errorwriteflag

  call elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)

  DB00_coli = B1_coli(q2,mm12,mm22)

  if (q2.ne.cnul.or.mm12.ne.cnul.or.mm22.ne.cnul) then
    DB00_coli = DB00_coli + (q2+mm12-mm22)*DB1_coli(q2,mm12,mm22)
  endif

  if (mm12.ne.cnul) then
    DB00_coli = DB00_coli + rtwo*mm12*DB0_coli(q2,mm12,mm22)
  endif

  if (mm12.ne.cnul.or.mm22.ne.cnul.or.q2.ne.cnul.or. &
      .not.ir_rat_terms) then
    DB00_coli = DB00_coli - rone/3
  end if

  DB00_coli = DB00_coli/6

  end

!***********************************************************************
  function Bn_coli(n,p2,m12,m22)
!***********************************************************************
!     tensor one loop integral B1..1 of rank n                         *
!     p2 outer momentum, m12,m22 inner masses                          *
!----------------------------------------------------------------------*
!     21.10.04 Ansgar Denner      last changed 24.10.08 ad             *
!***********************************************************************
  implicit none
  integer :: n,np
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  complex(kind=prec) :: Bn_coli
  complex(kind=prec) :: x1,x2,y1,y2,r,sum1,sum2,sum1a,sum2a
  logical :: finarg,nocalc
  integer :: i
  logical :: errorwriteflag

  np = n+1
  call elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)
  nocalc=.true.


! ---> special cases for small masses
  if (.not.finarg) then
    nocalc=.false.
    if (q2.eq.mm12.and.mm22.eq.rnul.and.q2.ne.rnul) then
      sum1 = rone/np
      do i=1,n
         sum1 = sum1 + rone/i
      end do
      Bn_coli = (-1)**np*(log(mm12*coliminfscale2/muuv2) &
          - rtwo*sum1)/np
    else if (q2.eq.mm22.and.mm12.eq.rnul.and.q2.ne.rnul) then
      Bn_coli = (-1)**np*(log(mm22*coliminfscale2/muuv2) &
          - rtwo/np)/np
    else if (q2.eq.rnul.and.mm12.eq.mm22.and.mm12.ne.rnul) then
      Bn_coli = (-1)**np*(log(mm12*coliminfscale2/muuv2))/np
    else if (q2.eq.rnul.and.mm12.eq.rnul.and.mm22.ne.rnul) then
      Bn_coli = (-1)**np*(log(mm22*coliminfscale2/muuv2) &
          - rone/np)/np
    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.ne.rnul) then
      sum1 = rone/np
      do i=1,n
         sum1 = sum1 + rone/i
      end do
      Bn_coli = (-1)**np*(log(mm12*coliminfscale2/muuv2) - sum1)/np
    else if (q2.ne.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      sum1 = rone/np
      do i=1,n
         sum1 = sum1 + rone/i
      end do
      Bn_coli = (-1)**np*(cln_coli(-q2*coliminfscale2/muuv2,-rone) &
                 - rone*sum1 - rone/np )/np
    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      Bn_coli = (-1)**np*log(muir2/muuv2)/np
#ifdef SING
      Bn_coli = Bn_coli + (-1)**np*delta1ir/np
#endif
    else
      nocalc=.true.
    endif
  endif

!---->  finite argument case
  if (nocalc) then
!---->  general case
    if (abs(q2).gt.calacc*abs(mm12+mm22)) then

      call crootse_coli(q2,mm12,mm22,x1,x2,y1,y2,r)

      if (abs(y1).gt.rhlf.and.abs(y2).gt.rhlf) then


        Bn_coli = (-1)**n*(-log(mm22/muuv2) &
             - fpve_coli(np,x1,y1,rone) - fpve_coli(np,x2,y2,-rone)) &
           /np

      elseif (abs(x1).lt.10*rone.and.abs(x2).lt.10*rone) then
        if(n.eq.0) then
          Bn_coli = rtwo - cln_coli(q2/muuv2,-rone) &
              + xlogxe_coli(-x1,-rone) + xlogxe_coli(-x2,rone) &
              - xlogxe_coli(y1,-rone) - xlogxe_coli(y2,rone)
        else
          sum1  = rone
          sum2  = rone
          sum1a = rone
          sum2a = rone
          do i=2,n+1
            sum1 = sum1*x1+rone
            sum2 = sum2*x2+rone
            sum1a = sum1a*x1 + rone/i
            sum2a = sum2a*x2 + rone/i
          end do
          Bn_coli = - cln_coli(q2/muuv2,-rone) &
              + x1**n*xlogxe_coli(-x1,-rone)+ sum1a &
              + x2**n*xlogxe_coli(-x2,rone) + sum2a &
              - sum1*xlogxe_coli(y1,-rone) &
              - sum2*xlogxe_coli(y2,rone)
        endif
        if (mm12.ne.rnul) Bn_coli = Bn_coli &
            - eta2_coli(-x1,-x2,mm12/q2,-rone,rone,-real(q2))
        Bn_coli = (-1)**n*Bn_coli/np


      elseif (abs(x1).gt.rhlf.and.abs(x2).gt.rhlf) then
        if(n.eq.0) then
          Bn_coli = (-1)**np*( log(mm12/muuv2) &
             + fpve_coli(1,y1,x1,-rone) + fpve_coli(1,y2,x2,rone) )/np
        else
          if (abs(x1).gt.10) then
            sum2  = rone
            sum2a = rhlf
            do i=2,n
              sum2 = sum2*x2+rone
              sum2a = sum2a*x2 + dfloat(i)/dfloat(i+1)
            end do
            sum2 = sum2*x2+rone
            Bn_coli = (-1)**np*( log(mm12/muuv2) &
                + fpve_coli(np,x1,y1,rone) - fpve_coli(0,x1,y1,rone) &
                + sum2*fpve_coli(1,y2,x2,rone) + sum2a )/np
          else
            sum1  = rone
            sum1a = rhlf
            do i=2,n
              sum1 = sum1*x1+rone
              sum1a = sum1a*x1 + dfloat(i)/dfloat(i+1)
            end do
            sum1 = sum1*x1+rone
            Bn_coli = (-1)**np*( log(mm12/muuv2) &
                + sum1*fpve_coli(1,y1,x1,-rone) + sum1a &
                + fpve_coli(np,x2,y2,-rone) &
                - fpve_coli(0,x2,y2,-rone))/np
          endif
        endif


      else
        call errB0_coli(q2,mm12,mm22,Bn_coli,'Bn_coli   ',12)
      endif
!---->  zero momentum
    elseif (abs(mm12-mm22).gt.calacc*abs(mm12+mm22)) then
      x2 = mm12/(mm12-mm22)
      y2 = mm22/(mm22-mm12)
      if(abs(y2).gt.rhlf) then
        Bn_coli = (-1)**np* &
            (log(mm22/muuv2) &
            + fpve_coli(np,x2,y2,real(mm22-mm12)))/np
      else
        sum2  = rone
        sum2a = rone
        do i=2,n+1
          sum2 = sum2*x2+rone
          sum2a = sum2a*x2 + rone/i
        end do
        Bn_coli = (-1)**np*(log(mm12/muuv2) &
            - sum2*yfpve_coli(0,x2,y2,real(mm22-mm12)) - sum2a)/np


      endif
    else if (mm22.ne.rnul) then
      Bn_coli = (-1)**np*log(mm22/muuv2)/np


    else
      call errB0_coli(q2,mm12,mm22,Bn_coli,'Bn_coli   ',20)
    endif
  endif

#ifdef SING

  Bn_coli = Bn_coli - (-1)**np*deltauv/np
#endif

  end

!***********************************************************************
  function B00n_coli(n,p2,m12,m22)
!***********************************************************************
!     tensor one loop integral B001..1 of rank n+2                     *
!     p2 outer momentum, m12,m22 inner masses                          *
!----------------------------------------------------------------------*
!     22.10.04 Ansgar Denner      last changed  21.10.08 ad            *
!***********************************************************************
  use coli_a0_<T>, only: A0_coli=>A0_<T>
  implicit none
  integer :: n
  complex(kind=prec) :: B00n_coli
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  logical :: finarg
  logical :: errorwriteflag

  call elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)
  if (finarg) then
    if (abs(q2)/(abs(q2)+abs(mm12+mm22)).gt.calacc) then
      B00n_coli = (q2+mm12-mm22)*Bn_coli(n+1,q2,mm12,mm22) &
          + rtwo*mm12*Bn_coli(n,q2,mm12,mm22) &
          +(-1)**n *( A0_coli(mm22) &
               +rtwo*(mm22 + mm12/(n+1) - q2/(n+3))/(n+2) )
      B00n_coli = B00n_coli/(6+rtwo*n)
    else
      B00n_coli = (mm12*Bn_coli(n,cnul,mm12,mm22) &
          +(-1)**n *( A0_coli(mm22) &
               + (mm22 + mm12/(n+1))/(n+2))) &
            /(4+rtwo*n)
    endif
  else
    B00n_coli = rnul
  endif
  end

!***********************************************************************
  function B0000n_coli(n,p2,m12,m22)
!***********************************************************************
!     tensor one loop integral B00001..1 of rank n+2                   *
!     p2 outer momentum, m12,m22 inner masses                          *
!----------------------------------------------------------------------*
!     22.10.04 Ansgar Denner      last changed  21.10.08 ad            *
!***********************************************************************
  use coli_a0_<T>, only: A0_coli=>A0_<T>
  implicit none
  integer :: n
  complex(kind=prec) :: B0000n_coli
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  logical :: finarg
  logical, save :: finarg2=.true.
  logical :: errorwriteflag

  if (finarg2.and.n.gt.2) then
#ifdef WARN
    call setErrFlag_coli(-1)
    call ErrOut_coli('B0000n_coli', &
        ' branch has not been tested', &
        errorwriteflag)
    if (errorwriteflag) then
      write(nerrout_coli,*) &
          'B0000n_coli : function has not been tested!!!!!'
      write(nerrout_coli,*) 'B0000n_coli : n = ',n
      finarg2 = .true.
    endif
#endif
  endif

  call elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)
  if (finarg) then
    if (abs(q2)/(abs(q2)+abs(mm12+mm22)).gt.calacc) then
      B0000n_coli = (q2+mm12-mm22)*B00n_coli(n+1,q2,mm12,mm22) &
          + rtwo*mm12*B00n_coli(n,q2,mm12,mm22) &
          +(-1)**n *( mm22*(A0_coli(mm22)+mm22/rtwo)/4 &
               + (mm22*mm22 &
                 + rtwo*mm12*(mm22+mm12/(n+1))/(n+2) &
                 + rtwo*q2/(n+4)*(q2/(n+5)-mm22-rtwo*mm12/(n+2))) &
                /(2*(n+3)) )
      B0000n_coli = B0000n_coli/(10*rone+rtwo*n)
    else
      B0000n_coli = (mm12*B00n_coli(n,cnul,mm12,mm22) &
          +(-1)**n *( mm22*(A0_coli(mm22)+mm22/rtwo)/4 &
               + (mm22*mm22 &
                 + rtwo*mm12*(mm22+mm12/(n+1))/(n+2)) /(4*(n+3)) ) ) &
            /(6+rtwo*n)


    endif
  else
    B0000n_coli = rnul
  endif
  end

!***********************************************************************
  function DBn_coli(n,p2,m12,m22)
!***********************************************************************
!     derivative of tensor one loop integral B1...1 of rank n          *
!     p2 outer momentum, m12,m22 inner masses squared                  *
!----------------------------------------------------------------------*
!     18.06.14 Ansgar Denner      last changed  18.06.14 ad            *
!***********************************************************************
  implicit none
  integer :: n,np
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  complex(kind=prec) :: DBn_coli
  complex(kind=prec) :: x1,x2,y1,y2,r,sum1,sum2,sum1a,sum2a
  complex(kind=prec) :: sum0
  integer :: i
  logical :: finarg,nocalc
  logical :: errorwriteflag

#ifdef WARN
  logical, save :: flag = .true.
#endif

#ifdef ADcode
  masterargs(1)=p2
  masterargs(2)=m12
  masterargs(3)=m22
  masterfname = 'DBn_coli '
  masterrank  = 1
#endif

  np = n+1
  call elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)
  nocalc=.true.

! ---> special cases for small masses
  if (.not.finarg) then
    nocalc=.false.
    if (q2.eq.mm12.and.m22.eq.rnul.and.q2.ne.rnul) then
      sum0=rone
      do i=2,np
        sum0 = sum0 + rone/i
      enddo
      DBn_coli = (rtwo*sum0 + log(muir2/(mm12*coliminfscale2))) &
                 /(rtwo*q2)
#ifdef SING
      DBn_coli = DBn_coli + delta1ir/(rtwo*q2)
#endif
      DBn_coli = (-1)**np*DBn_coli

    else if (q2.eq.mm22.and.m12.eq.rnul.and.q2.ne.rnul) then
      if (n.gt.0) then
        DBn_coli = (-1)**n/(q2*n*np)

      else
        DBn_coli = -(rone + rhlf*log(muir2/(mm22*coliminfscale2)))/q2
#ifdef SING
        DBn_coli = DBn_coli - rhlf*delta1ir/q2
#endif

      endif
    else if (q2.eq.rnul.and.mm12.eq.mm22.and.mm12.ne.rnul) then
      DBn_coli = (-1)**n/(mm12*(n+2)*(n+3))

    else if (q2.eq.rnul.and.mm12.eq.rnul.and.mm22.ne.rnul) then
      DBn_coli = (-1)**n/(mm22*np*(n+2))

    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.ne.rnul) then
      DBn_coli = (-1)**n/(mm12*(n+2))

    else if (q2.ne.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      DBn_coli = (-1)**np/(np*q2)

    else if (q2.eq.rnul.and.mm22.eq.rnul.and.mm12.eq.rnul) then
      DBn_coli = cnul

#ifdef WARN
      if(flag) then
      call setErrFlag_coli(-1)
      call ErrOut_coli('DBn_coli',
 &        ' DBn_coli for three vanishing arguments not defined',
 &        errorwriteflag)

        write(nerrout_coli,*)
 &          ' DBn_coli for three vanishing arguments not defined '
        flag=.false.
      endif
#endif
    else
      nocalc=.true.
    endif

  endif

!---->  finite argument case
  if (nocalc) then
! IR-singular case
    if(p2.eq.m12.and.m22.eq.rnul.or. &
        n.eq.0.and.p2.eq.m22.and.m12.eq.rnul) then
      sum0=rone
      do i=2,np
        sum0 = sum0 + rone/i
      enddo
      DBn_coli = (rtwo*sum0 + log(muir2/p2))/(rtwo*p2)
#ifdef SING
      DBn_coli = DBn_coli + delta1ir/(rtwo*p2)
#endif
      DBn_coli = (-1)**np*DBn_coli

!---->  general case
    else if (abs(q2).gt.calacc*abs(mm12+mm22)) then
      call crootse_coli(q2,mm12,mm22,x1,x2,y1,y2,r)


      if (abs(x1-x2).gt.sqrt(calacc)*abs(x1+x2)) then

        if (abs(x1).gt.rone/10.or.abs(x2).gt.rone/10) then
          DBn_coli = (-1)**n * (yfpve_coli(np,x2,y2,-rone) &
              - yfpve_coli(np,x1,y1,rone))/r

        else if (n.ne.0) then
          DBn_coli = (x2*xyfpve_coli(n-1,x2,y2,-rone) &
              -x1*xyfpve_coli(n-1,x1,y1,rone)) / r &
               -(x1+x2-rone/np)/(n*q2)

          DBn_coli = (-1)**n *DBn_coli

        else  ! n =0

          DBn_coli = (xyfpve_coli(0,x2,y2,-rone) &
              - xyfpve_coli(0,x1,y1,rone))/r &
                   -rone/q2

        endif

      else if (abs(x1-x2).gt.calacc*abs(x1+x2)) then

        DBn_coli = (-1)**n * (yfpve_coli(np,x2,y2,-rone) &
            - yfpve_coli(np,x1,y1,rone))/r

      elseif (abs(x1).gt.10*rone) then
        DBn_coli = ( -(np-(n+2)*x1)*fpve_coli((n+2),x1,y1,rone) &
            - np/(n+rtwo) ) &
                /(q2*x1*x1) * (-1)**(n)

      elseif (abs(y1).gt.calacc.and.(abs(x1).gt.calacc.or.n.ne.0)) &
              then
        DBn_coli = ( ((n+2)*x1-np)*fpve_coli(n,x1,y1,rone) &
           - (n+rtwo)/(n+rone) ) /q2  * (-1)**(n)


      elseif (mm22.eq.rnul.and.q2.eq.mm12) then
        sum0=rone
        do i=2,np
          sum0 = sum0 + rone/i
        enddo
        DBn_coli = (rtwo*sum0 + log(m22/q2))/(rtwo*q2)
        DBn_coli = (-1)**np*DBn_coli

      elseif (n.eq.0.and.p2.eq.mm22.and.mm12.eq.rnul) then
        DBn_coli = (rtwo + log(m12/q2))/(rtwo*q2)
        DBn_coli = (-1)**np*DBn_coli

      else

        call errB0_coli(q2,mm12,mm22,DBn_coli,'DBn_coli  ',12)
      endif
!---->  zero momentum
    elseif (abs(mm12-mm22).gt.calacc*abs(mm12+mm22)) then
      x2 = mm12/(mm12-mm22)
      y2 = mm22/(mm22-mm12)
      if (abs(x2).lt.10*rone) then
        DBn_coli = (-1)**n*(rone/(n+2) + yfpve_coli(np,x2,y2,real(mm22-mm12))) &
            /(mm12-mm22)

      else
        DBn_coli = (-1)**n*(y2*fpve_coli(n+2,x2,y2,real(mm22-mm12)) &
                    + rone/(n+2))/mm12

      endif
    else if (mm12.ne.rnul) then
      DBn_coli  = (-1)**n/((n+2)*(n+3)*mm12)

    else
      call errB0_coli(q2,mm12,mm22,DBn_coli,'DBn_coli  ',20)
    endif
  endif

  end



!***********************************************************************
  subroutine elminf2iv_coli(p2,m12,m22,q2,mm12,mm22,finarg)
!***********************************************************************
!     eliminate small masses squared in vertex   p2,m12,m22            *
!----------------------------------------------------------------------*
!     16.03.04 Ansgar Denner                                           *
!***********************************************************************
  implicit none
  complex(kind=prec) :: p2
  complex(kind=prec) :: m12,m22
  complex(kind=prec) :: q2
  complex(kind=prec) :: mm12,mm22
  complex(kind=prec) :: p2s
  complex(kind=prec) :: mm12s,mm22s
  logical :: finarg

  p2s  = elimminf2_coli(p2)
  mm12s = elimminf2_coli(m12)
  mm22s = elimminf2_coli(m22)

  if(p2s.ne.rnul.or.mm12s.ne.rnul.or.mm22s.ne.rnul) then
    q2 = p2s
    mm12 = mm12s
    mm22 = mm22s
    finarg = .true.
  else
    q2 = p2
    mm12 = m12
    mm22 = m22
    finarg  = .false.
  endif
  end

!***********************************************************************
  subroutine errB0_coli(q2,m12,m22,value,name,code)
!***********************************************************************
!     error messages for B0 functions                                  *
!     q2 outer momentum      m12,m22 inner masses squared              *
!----------------------------------------------------------------------*
!     19.08.93 Ansgar Denner      last changed  21.10.08 ad            *
!**************************:********************************************
  implicit none
  complex(kind=prec) :: value
  complex(kind=prec) :: q2
  complex(kind=prec) :: m12,m22
  character :: name*9
  integer :: code
  logical :: errorwriteflag

!      if (code.eq.16) then
!        call ErrOut_coli(name,' not well defined',errorwriteflag)
!      elseif (code.eq.12) then
!        call ErrOut_coli(name,' case not implemented',errorwriteflag)
!      elseif (code.eq.4) then
!        call ErrOut_coli(name,' possibly not well defined',
!     &        errorwriteflag)
!      elseif (code.eq.20) then
!        call ErrOut_coli(name,' not defined for zero arguments',
!     &        errorwriteflag)
!      elseif (code.eq.24) then
!        call ErrOut_coli(name,' not defined for almost zero masses',
!     &        errorwriteflag)
!      elseif (code.eq.28) then
!        call ErrOut_coli(name,
!     &      ' not defined for this case of small masses',
!     &        errorwriteflag)
!      endif
!      if(errorwriteflag) then
!        write(nerrout_coli,*) ' q2,m12,m22 = ',q2,m12,m22
!        write(nerrout_coli,*) '  ',name,'     = ',value
!      endif
  end


!***********************************************************************
  function fpve_coli(n,x,y,eps)
!***********************************************************************
!     passarino and veltmans f(n,x) function                           *
!     y = 1-x,  x  = x + i eps                                         *
!----------------------------------------------------------------------*
!     21.07.93 Ansgar Denner      last changed  16.03.04 ad            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: x,y,fpve_coli
  real(kind=prec)    :: eps
  integer            :: n,m

  if(abs(x).lt.10*rone) then
    if (n.eq.0) then
      fpve_coli = -cln_coli(-y/x,eps)
    elseif (x.ne.cnul) then
      fpve_coli = -x**n*cln_coli(-y/x,eps)
      do m=1,n
        fpve_coli = fpve_coli - x**(n-m)/m
      enddo
    else
      fpve_coli = -rone/n
    endif
  else
    fpve_coli = rnul
    do m=1,10
      fpve_coli = fpve_coli + rone/((m+n)*x**m)
      if (abs(rone/(x**m*fpve_coli)).lt.calacc**2) return
    enddo
  endif
  end

!***********************************************************************
  function yfpve_coli(n,x,y,eps)
!***********************************************************************
!     passarino and veltmans f(n,x) function times (1-x) = y           *
!     x  = x + i eps                                                   *
!----------------------------------------------------------------------*
!     19.08.93 Ansgar Denner      last changed  16.03.04 ad            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: x,y,yfpve_coli
  real(kind=prec)    :: eps
  integer            :: n

  if(abs(y).eq.rnul) then
    yfpve_coli = rnul
  else
    yfpve_coli = y*fpve_coli(n,x,y,eps)
  endif
  end

!***********************************************************************
  function xyfpve_coli(n,x,y,eps)
!***********************************************************************
!     passarino and veltmans f(n,x) function times x*(1-x) = x*y       *
!     x  = x + i eps                                                   *
!----------------------------------------------------------------------*
!     20.09.04 Ansgar Denner      last changed  20.09.04 ad            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: x,y,xyfpve_coli
  real(kind=prec)    :: eps
  integer            :: n

  if(abs(y).eq.rnul.or.abs(x).eq.rnul) then
    xyfpve_coli = rnul
  else
    xyfpve_coli = x*y*fpve_coli(n,x,y,eps)
  endif
  end

!***********************************************************************
  function xlogxe_coli(x,eps)
!***********************************************************************
!     x*log(x),    x  = x + i eps                                      *
!----------------------------------------------------------------------*
!     19.08.93 Ansgar Denner      last changed  16.03.04 ad            *
!***********************************************************************
  implicit none
  complex(kind=prec) :: x,xlogxe_coli
  real(kind=prec)    :: eps

  if(abs(x).eq.rnul) then
    xlogxe_coli = rnul
  else
    xlogxe_coli = x*cln_coli(x,eps)
  endif
  end

end module
