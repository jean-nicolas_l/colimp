#import "prectemplate.inc"

module prectargets_<T>
  use prectargets_impacc_<T>, only: impacc, calacc
  use prectargets_acc_<T>, only: acc
  implicit none
  integer, parameter :: <T> = @T
end module prectargets_<T>
