#import "prectemplate.inc"
    use coli_aux_<T>, only: setmuuv2_<T>=>setmuuv2_coli
    use coli_aux_<T>, only: getmuuv2_<T>=>getmuuv2_coli
    use coli_aux_<T>, only: setmuir2_<T>=>setmuir2_coli
    use coli_aux_<T>, only: getmuir2_<T>=>getmuir2_coli
    use coli_aux_<T>, only: setdeltauv_<T>=>setdeltauv_coli
    use coli_aux_<T>, only: setdeltair_<T>=>setdeltair_coli
    use coli_aux_<T>, only: getdeltauv_<T>=>getdeltauv_coli
    use coli_aux_<T>, only: getdeltair_<T>=>getdeltair_coli
    use coli_aux_<T>, only: setminf2_<T>=>setminf2_coli
