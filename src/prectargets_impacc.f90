#import "prectargets_impacc.inc"

module prectargets_impacc_<T>
  implicit none
  real(kind=8), parameter :: impacc = @T
  real(kind=8), parameter :: calacc = @T
end module prectargets_impacc_<T>
