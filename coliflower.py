#!/usr/bin/env python
#  File coliflower.py is part of COLLIER and Coliflower
#
#  Copyright (C) 2015, 2016   Ansgar Denner, Stefan Dittmaier, Lars Hofer
#
#  COLLIER is licenced under the GNU GPL version 3, see COPYING for details.
#
#  Multiprecision support and python interface by J.-N. Lang

import sys
from ctypes import CDLL,c_int,c_float,c_double,c_longdouble,c_char_p,c_void_p,byref,POINTER
import numpy as np

if sys.platform.startswith("darwin"):
  coliflowerlib = CDLL("libcoliflower.dylib")
else:
  coliflowerlib = CDLL("libcoliflower.so")

class ColiFlowerMeta(type):
  def __init__(cls, name, bases, dict):
    prec_types = {'sp': (c_float,), 'dp': (c_double,), 'lp': (c_longdouble,)}
    cls.A0_func = {}
    cls.B0_func = {}
    cls.bubble_func = {}
    cls.C0_func = {}
    cls.D0_func = {}
    cls.setmuuv2_func = {}
    cls.getmuuv2_func = {}
    cls.setmuir2_func = {}
    cls.getmuir2_func = {}
    cls.setdeltauv_func = {}
    cls.getdeltauv_func = {}
    cls.setdeltair_func = {}
    cls.getdeltair_func = {}
    cls.setminf2_func = {}
    for pt in prec_types:
      # A0
      func = 'A0_coliflower_' + pt
      prec_fp = prec_types[pt][0]
      setattr(cls, func, getattr(coliflowerlib, func))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [POINTER(prec_fp), POINTER(prec_fp)]
      cls.A0_func[pt] = getattr(cls, func)

      # B0
      func = 'B0_coliflower_' + pt
      prec_fp = prec_types[pt][0]
      setattr(cls, func, getattr(coliflowerlib, func))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [
            POINTER(prec_fp),
            prec_fp, POINTER(prec_fp),POINTER(prec_fp)
          ]
      cls.B0_func[pt] = getattr(cls, func)

      # bubble
      func = 'bubble_coliflower_' + pt
      prec_fp = prec_types[pt][0]
      setattr(cls, func, getattr(coliflowerlib, func))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [
            POINTER(prec_fp), POINTER(prec_fp), POINTER(prec_fp), POINTER(prec_fp),
            c_int, prec_fp, POINTER(prec_fp),POINTER(prec_fp)
          ]
      cls.bubble_func[pt] = getattr(cls, func)

      # C0
      func = 'C0_coliflower_' + pt
      prec_fp = prec_types[pt][0]
      setattr(cls, func, getattr(coliflowerlib, func))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [
            POINTER(prec_fp),
            prec_fp, prec_fp, prec_fp,
            POINTER(prec_fp),POINTER(prec_fp),POINTER(prec_fp)
          ]
      cls.C0_func[pt] = getattr(cls, func)

      # D0
      func = 'D0_coliflower_' + pt
      prec_fp = prec_types[pt][0]
      setattr(cls, func, getattr(coliflowerlib, func))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [
            POINTER(prec_fp),
            prec_fp, prec_fp, prec_fp, prec_fp, prec_fp, prec_fp,
            POINTER(prec_fp), POINTER(prec_fp), POINTER(prec_fp), POINTER(prec_fp)
          ]
      cls.D0_func[pt] = getattr(cls, func)


      # setting/getting scales
      func = 'setmuuv2_' + pt
      funccf = 'setmuuv2_coliflower_' + pt
      setattr(cls, func, getattr(coliflowerlib, funccf))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [prec_fp]
      cls.setmuuv2_func[pt] = getattr(cls, func)

      func = 'getmuuv2_' + pt
      funccf = 'getmuuv2_coliflower_' + pt
      setattr(cls, func, getattr(coliflowerlib, funccf))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [POINTER(prec_fp)]
      cls.getmuuv2_func[pt] = getattr(cls, func)

      func = 'setmuir2_' + pt
      funccf = 'setmuir2_coliflower_' + pt
      setattr(cls, func, getattr(coliflowerlib, funccf))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [prec_fp]
      cls.setmuir2_func[pt] = getattr(cls, func)

      func = 'getmuir2_' + pt
      funccf = 'getmuir2_coliflower_' + pt
      setattr(cls, func, getattr(coliflowerlib, funccf))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [POINTER(prec_fp)]
      cls.setmuir2_func[pt] = getattr(cls, func)

      func = 'setdeltauv_' + pt
      funccf = 'setdeltauv_coliflower_' + pt
      setattr(cls, func, getattr(coliflowerlib, funccf))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [prec_fp]
      cls.setdeltauv_func[pt] = getattr(cls, func)

      func = 'getdeltauv_' + pt
      funccf = 'getdeltauv_coliflower_' + pt
      setattr(cls, func, getattr(coliflowerlib, funccf))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [POINTER(prec_fp)]
      cls.getdeltauv_func[pt] = getattr(cls, func)

      func = 'setdeltair_' + pt
      funccf = 'setdeltair_coliflower_' + pt
      setattr(cls, func, getattr(coliflowerlib, funccf))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [prec_fp, prec_fp]
      cls.setdeltair_func[pt] = getattr(cls, func)

      func = 'getdeltair_' + pt
      funccf = 'getdeltair_coliflower_' + pt
      setattr(cls, func, getattr(coliflowerlib, funccf))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [POINTER(prec_fp), POINTER(prec_fp)]
      cls.getdeltair_func[pt] = getattr(cls, func)

      func = 'setminf2_' + pt
      funccf = 'setminf2_coliflower_' + pt
      setattr(cls, func, getattr(coliflowerlib, funccf))
      getattr(cls, func).restype = c_void_p
      getattr(cls, func).argtypes = [prec_fp]
      cls.setminf2_func[pt] = getattr(cls, func)

    super(ColiFlowerMeta, cls).__init__(name, bases, dict)

class ColiFlower(metaclass=ColiFlowerMeta):

  initcoliflower = coliflowerlib.init_coliflower
  initcoliflower.restype = c_void_p
  initcoliflower.argtypes = None

  def __init__(self,prec="dp"):
    self.__prec = prec.lower()
    self.initcoliflower()
    if self.__prec not in {"sp", "dp", "lp"}:
      raise Exception("Target precision type not supported.")

  @property
  def real_type_c(self):
    return {'sp': c_float,
            'dp': c_double,
            'lp': c_longdouble,
            'qp': None}[self.__prec]

  @property
  def real_type(self):
    return {'sp': np.float32,
            'dp': np.float64,
            'lp': np.float128,
            'qp': None}[self.__prec]

  @property
  def complex_type(self):
    return {'sp': np.complex64,
            'dp': np.complex128,
            'lp': np.complex256,
            'qp': None}[self.__prec]

  def A0(self, m02):
    """
    Computes the 1-point integral.

    Parameters
    ----------
    m02 : complex(FP)
        Complex mass with floating point precision `prec`

    Returns
    -------
    numpy.complex
        Returns results in complex precision `prec`, i.e. either
        numpy.complex64, numpy.complex128, numpy.complex256.

    >>> colisp = ColiFlower(prec="SP")
    >>> res = colisp.A0(complex(100,-10.))
    >>> np.round(res,3)
    (-360.018+46.068j)
    >>> colidp = ColiFlower(prec="DP")
    >>> res = colidp.A0(complex(100,-10.))
    >>> np.round(res,13)
    (-360.0178486165559+46.068318763263j)
    >>> colilp = ColiFlower(prec="LP")
    >>> res = colilp.A0(complex(100,-10.))
    >>> np.round(res,15)
    (-360.017848616555921+46.068318763262957j)
    """
    res = (self.real_type_c*(2))()
    intmasses_c = (self.real_type_c*(2))()
    intmasses_c[0] = self.real_type_c(m02.real)
    intmasses_c[1] = self.real_type_c(m02.imag)
    self.A0_func[self.__prec](res, intmasses_c)
    ret = np.frombuffer(res, dtype=self.complex_type)
    return ret[0]

  def B0(self, p2, m02, m12):
    """
    Computes the 2-point integral.

    Parameters
    ----------
    p2 : complex(FP)
        Real (momentum squared) value with floating point precision `prec`
    m02, m12 : complex(FP)
        Complex (masses) value with floating point precision `prec`

    Returns
    -------
    numpy.complex
        Returns results in complex precision `prec`, i.e. either
        numpy.complex64, numpy.complex128, numpy.complex256.

    >>> colilp = ColiFlower(prec="LP")
    >>> res = colilp.B0(10000,complex(10.,0.),complex(20.,0.))
    >>> np.round(res,16)
    (-7.187979587294492+3.1321552714204996j)

    """
    res = (self.real_type_c*(2))()
    m02_c = (self.real_type_c*(2))()
    m02_c[0] = self.real_type_c(m02.real)
    m02_c[1] = self.real_type_c(m02.imag)
    m12_c = (self.real_type_c*(2))()
    m12_c[0] = self.real_type_c(m12.real)
    m12_c[1] = self.real_type_c(m12.imag)
    self.B0_func[self.__prec](res, self.real_type_c(p2), m02_c, m12_c)
    ret = np.frombuffer(res, dtype=self.complex_type)
    return ret[0]

  def bubble(self, rank, p2, m02, m12):
    """
    Computes the 2-point integrals B0, B1, B00, B22.

    Parameters
    ----------
    rank : int
        Integer that determines which of the 2-point integrals are computed.
        0: only B0
        1: B0 and B1
        2: all
    p2 : complex(FP)
        Real (momentum squared) value with floating point precision `prec`
    m02, m12 : complex(FP)
        Complex (masses) value with floating point precision `prec`

    Returns
    -------
    (numpy.complex, numpy.complex, numpy.complex, numpy.complex)
        Returns results in complex precision `prec`, i.e. either
        numpy.complex64, numpy.complex128, numpy.complex256.

    >>> colilp = ColiFlower(prec="LP")
    >>> B0,B1,B00,B11 = colilp.bubble(2, 10000,complex(10.,0.),complex(20.,0.))
    >>> np.round(B0,16)
    (-7.187979587294492+3.1321552714204996j)
    >>> np.round(B1,16)
    (3.5917402435806557-1.5645115580745396j)
    >>> np.round(B00,12)
    (5399.079577136741-2594.471226622707j)
    >>> np.round(B11,16)
    (-2.3359779416557657+1.0409206459205032j)

    """
    res_B0 = (self.real_type_c*(2))()
    res_B1 = (self.real_type_c*(2))()
    res_B00 = (self.real_type_c*(2))()
    res_B11 = (self.real_type_c*(2))()
    m02_c = (self.real_type_c*(2))()
    m02_c[0] = self.real_type_c(m02.real)
    m02_c[1] = self.real_type_c(m02.imag)
    m12_c = (self.real_type_c*(2))()
    m12_c[0] = self.real_type_c(m12.real)
    m12_c[1] = self.real_type_c(m12.imag)
    self.bubble_func[self.__prec](res_B0, res_B1, res_B00, res_B11,
                                  c_int(rank),
                                  self.real_type_c(p2), m02_c, m12_c)
    ret_B0 = np.frombuffer(res_B0, dtype=self.complex_type)
    ret_B1 = np.frombuffer(res_B1, dtype=self.complex_type)
    ret_B00 = np.frombuffer(res_B00, dtype=self.complex_type)
    ret_B11 = np.frombuffer(res_B11, dtype=self.complex_type)
    if rank == 0:
      return ret_B0[0]
    elif rank == 1:
      return ret_B0[0],ret_B1[0]
    elif rank == 2:
      return ret_B0[0], ret_B1[0], ret_B00[0], ret_B11[0]

  def C0(self, p10, p21, p20, m02, m12, m22):
    """
    Computes the 3-point integral.

    Parameters
    ----------
    p10, p21, p20 : real(FP)
        Real (momentum squared) value with floating point precision `prec`
    m02, m12, m22 : complex(FP)
        Complex (masses) value with floating point precision `prec`

    Returns
    -------
    numpy.complex
        Returns results in complex precision `prec`, i.e. either
        numpy.complex64, numpy.complex128, numpy.complex256.

    >>> colilp = ColiFlower(prec="LP")
    >>> res = colilp.C0(-10., 10000., -10.,\
                  complex(0.,0.),complex(20.,0.),complex(300.,0.))
    >>> np.round(res, 18)
    (0.001043437819953292-0.002893990265101249j)
    """
    res = (self.real_type_c*(2))()
    m02_c = (self.real_type_c*(2))()
    m02_c[0] = self.real_type_c(m02.real)
    m02_c[1] = self.real_type_c(m02.imag)
    m12_c = (self.real_type_c*(2))()
    m12_c[0] = self.real_type_c(m12.real)
    m12_c[1] = self.real_type_c(m12.imag)
    m22_c = (self.real_type_c*(2))()
    m22_c[0] = self.real_type_c(m22.real)
    m22_c[1] = self.real_type_c(m22.imag)
    self.C0_func[self.__prec](res,
        self.real_type_c(p10),
        self.real_type_c(p21),
        self.real_type_c(p20),
        m02_c, m12_c, m22_c)
    ret = np.frombuffer(res, dtype=self.complex_type)
    return ret[0]


  def D0(self, p10, p21, p32, p30, p20, p31, m02, m12, m22, m32):
    """
    Computes the 4-point integral.

    Parameters
    ----------
    p10, p21, p32, p30, p20, p31 : real(FP)
        Real (momentum squared) value with floating point precision `prec`
    m02, m12, m22, m32 : complex(FP)
        Complex (masses) value with floating point precision `prec`

    Returns
    -------
    numpy.complex
        Returns results in complex precision `prec`, i.e. either
        numpy.complex64, numpy.complex128, numpy.complex256.

    >>> colilp = ColiFlower(prec="LP")
    >>> res = colilp.D0(0., 284842.09968989424, 0., 146892.67583825623,\
        -86930.363195152982, -481334.86127669609, \
        complex(0.,0.),complex(0.,0.),complex(0.,0.),complex(0.,0.))
    >>> np.round(res, 24)
    (6.139969256239830103e-10+3.0034064719271679025e-06j)
    """
    res = (self.real_type_c*(2))()
    m02_c = (self.real_type_c*(2))()
    m02_c[0] = self.real_type_c(m02.real)
    m02_c[1] = self.real_type_c(m02.imag)
    m12_c = (self.real_type_c*(2))()
    m12_c[0] = self.real_type_c(m12.real)
    m12_c[1] = self.real_type_c(m12.imag)
    m22_c = (self.real_type_c*(2))()
    m22_c[0] = self.real_type_c(m22.real)
    m22_c[1] = self.real_type_c(m22.imag)
    m32_c = (self.real_type_c*(2))()
    m32_c[0] = self.real_type_c(m32.real)
    m32_c[1] = self.real_type_c(m32.imag)
    self.D0_func[self.__prec](res,
        self.real_type_c(p10),
        self.real_type_c(p21),
        self.real_type_c(p32),
        self.real_type_c(p30),
        self.real_type_c(p20),
        self.real_type_c(p31),
        m02_c, m12_c, m22_c, m32_c)
    ret = np.frombuffer(res, dtype=self.complex_type)
    return ret[0]

  def setmuuv2(self, muuv2):
    """
    Sets the muUV^2 scale.

    Parameters
    ----------
    muuv2 : real(FP)
        Real value with floating point precision `prec`
    """
    self.setmuuv2_func[self.__prec](self.real_type_c(muuv2))

  def getmuuv2(self):
    """
    Returns the muUV^2 scale.

    Returns
    -------
    numpy.float
        Real value with floating point precision `prec`

    >>> colidp = ColiFlower()
    >>> colidp.getmuuv2()
    1.0
    >>> colidp.setmuuv2(10001.3)
    >>> colidp.getmuuv2()
    10001.3
    """
    res = (self.real_type_c*(1))()
    self.getmuuv2_func[self.__prec](res)
    ret = np.frombuffer(res, dtype=self.real_type)[0]
    return ret

  def setmuir2(self, muir2):
    """
    Sets the muIR^2 scale.

    Parameters
    ----------
    muir2 : real(FP)
        Real value with floating point precision `prec`
    """
    self.setmuir2_func[self.__prec](self.real_type_c(muir2))

  def getmuir2(self):
    """
    Returns the muIR^2 scale.

    Returns
    -------
    numpy.float
        Real value with floating point precision `prec`

    >>> colidp = ColiFlower()
    >>> colidp.getmuuv2()
    1.0
    >>> colidp.setmuuv2(-77.4)
    >>> colidp.getmuuv2()
    -77.4
    """
    res = (self.real_type_c*(1))()
    self.getmuir2_func[self.__prec](res)
    ret = np.frombuffer(res, dtype=self.real_type)[0]
    return ret

  def setdeltauv(self, deltauv):
    """
    Sets the UV Pole part \DeltaUV.

    Parameters
    ----------
    deltauv : float/numpy.float
        Real value with floating point precision `prec`
    """
    self.setdeltauv_func[self.__prec](self.real_type_c(deltauv))

  def getdeltauv(self):
    """
    Gets the UV Pole part \DeltaUV.

    Returns
    -------
    deltauv : real(FP)
        Real value with floating point precision `prec`

    >>> colisp = ColiFlower(prec="sp")
    >>> colisp.getdeltauv()
    0.0
    >>> colisp.setdeltauv(1.3)
    >>> colisp.getdeltauv()
    1.3
    """
    res = (self.real_type_c*(1))()
    self.getdeltauv_func[self.__prec](res)
    ret = np.frombuffer(res, dtype=self.real_type)[0]
    return ret

  def setdeltair(self, deltair1, deltair2):
    """
    Sets the IR Pole parts \DeltaIR_1 and \DeltaIR_2.

    Parameters
    ----------
    deltair1, deltair1: float/numpy.float
        Real value with floating point precision `prec`
    """

    self.setdeltair_func[self.__prec](self.real_type_c(deltair1),
                                      self.real_type_c(deltair2))

  def getdeltair(self):
    """
    Gets the IR Pole part \DeltaIR_1, \DeltaIR_2.

    Returns
    -------
    deltauv : real(FP)
        Real value with floating point precision `prec`

    >>> colisp = ColiFlower(prec="sp")
    >>> colisp.getdeltair()
    (0.0, 0.0)
    >>> colisp.setdeltair(1.3, 77.1)
    >>> colisp.getdeltair()
    (1.3, 77.1)
    """
    res1 = (self.real_type_c*(1))()
    res2 = (self.real_type_c*(1))()
    self.getdeltair_func[self.__prec](res1,res2)
    ret1 = np.frombuffer(res1, dtype=self.real_type)[0]
    ret2 = np.frombuffer(res2, dtype=self.real_type)[0]
    return ret1, ret2


  def setminf2(self, m2):
    """
    Sets the value of some mass^2 to be treated infinitesimally.

    Parameters
    ----------
    m2 : real(FP)
        Real value with floating point precision `prec`
    >>> cf = ColiFlower()
    >>> m02 = complex(1./3, 0.)
    >>> np.round(cf.D0(0., 0., 1.5, 1.5, 16., -12.3243, m02, m02, m02, m02),10)
    (-0.1762572413+0.0736791718j)
    >>> cf.setminf2(1./3)
    >>> np.round(cf.D0(0., 0., 1.5, 1.5, 16., -12.3243, m02, m02, m02, m02),10)
    (-0.1550987401+0.0127524453j)
    """
    self.setminf2_func[self.__prec](self.real_type_c(m2))

if __name__ == "__main__":
  import doctest
  doctest.testmod()
