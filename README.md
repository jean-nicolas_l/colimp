<img
src="extra/logo.png"
raw=true
alt="ColiFlower"
width="578"
height="150"
/>


# COLIflower: COLI with Multiple Precision Arithmetics

* * *

Coliflower is a Fortran90 version of COLI that implements scalar 1-loop
integrals in single, double, long double and quadruple precision.
If you use Coliflower, please cite [Collier][2].

## Compilation

Run

    mkdir build && cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release
    make

Running the tests:

    make check

Custom flags via:

    -DFortran_FLAGS="-O2 -cpp"
    -DFortran_FLAGS_RELEASE="-Ofast -cpp"
    -DFortran_FLAGS_DEBUG="-O0 -cpp -g"

## Usage

An example is provided in demo/demo.f90 which can be compiled and run via

    cd build
    make demo
    ./demo


As for the python interface simply use

    from coliflower import ColiFlower
    coli = ColiFlower()
    # compute a 2-point function
    B0 = coli.B0(10000,complex(10.,0.),complex(20.,0.))
    print(B0)

See also the test cases in tests/.

* * *

## External dependencies

COLIflower uses CMAKE as the building system but has no other required dependencies.


+ [CMake][1] build system
+ Optionally, [Collier][2] and [OneLoop][3] (if they are found) for running the tests.

* * *

[1]: https://cmake.org/                         "CMake"
[2]: http://collier.hepforge.org/               "collier@hepforge"
[3]: https://bitbucket.org/hameren/oneloop/     "OneLOop@bitbucket"
