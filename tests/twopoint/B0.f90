program B0test
  use coliflower, only: initcoli,B0,dp,lp,qp,setmuir2,setmuuv2
  use test_tools, only: compare
  implicit none

  complex(kind=qp) :: p10_qp, m02_qp, m12_qp
  complex(kind=lp) :: p10_lp, m02_lp, m12_lp
  complex(kind=dp) :: p10_dp, m02_dp, m12_dp

  real(kind=qp) :: rescale = 1.3_qp,acc_resc,B0tmp


  p10_qp = complex(10000._qp,0._qp)
  m02_qp = complex(10._qp,0._qp)
  m12_qp = complex(20._qp,0._qp)

  p10_dp = p10_qp
  m02_dp = m02_qp
  m12_dp = m12_qp

  p10_lp = p10_qp
  m02_lp = m02_qp
  m12_lp = m12_qp

  call initcoli

  call compare(B0(p10_dp,m02_dp,m12_dp), B0(p10_lp,m02_lp,m12_lp), 15)
  call compare(B0(p10_dp,m02_dp,m12_dp), B0(p10_qp,m02_qp,m12_qp), 15)
  call compare(B0(p10_lp,m02_lp,m12_lp), B0(p10_qp,m02_qp,m12_qp), 18)

  B0tmp = B0(p10_qp,m02_qp,m12_qp)
  call setmuuv2(rescale**2)
  call setmuir2(rescale**2)
  acc_resc = B0(p10_qp*rescale**2,m02_qp*rescale**2,m12_qp*rescale**2)/B0tmp

  call compare(acc_resc, 1._qp, 31)
  
end program
