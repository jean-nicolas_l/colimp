program C0test
  use coliflower, only: initcoli,C0,sp,dp,lp,qp,setmuir2,setmuuv2,setminf2
  use test_tools, only: compare
#ifdef WITH_COLLIER
  use collier
#endif
  implicit none

  complex(kind=qp) :: c0res_qp
  complex(kind=dp) :: c0res_dp
  complex(kind=qp) :: p12_qp, p23_qp, p13_qp, m02_qp, m12_qp, m22_qp
  complex(kind=lp) :: p12_lp, p23_lp, p13_lp, m02_lp, m12_lp, m22_lp
  complex(kind=dp) :: p12_dp, p23_dp, p13_dp, m02_dp, m12_dp, m22_dp
  complex(kind=sp) :: p12_sp, p23_sp, p13_sp, m02_sp, m12_sp, m22_sp
  complex(kind=dp), allocatable :: minfcll(:)

  real(kind=qp) :: rescale = 1.3_qp,acc_resc

  integer :: acc_coli_cmp, acc_resc_cmp

  integer :: kconf, mconf

  do kconf = 1, 2
    do mconf = 1, 3
      call set_kinematics(kconf)
      call set_masses(mconf)
      call runcheck
    end do
  end do

  contains

  subroutine set_kinematics(setup)
    integer :: setup
    write(*,*) "Setting kinematics", setup
    select case (setup)
      case (1)
        p12_qp = complex(10000._qp,0._qp)
        p23_qp = complex(-10._qp,0._qp)
        p13_qp = complex(-10,0._qp)
        acc_coli_cmp = 13
        acc_resc_cmp = 31
      case (2)
        p12_qp = complex(-10,0._qp)
        p23_qp = complex(10000._qp,0._qp)
        p13_qp = complex(-10,0._qp)
        acc_coli_cmp = 13
        acc_resc_cmp = 31
    end select

  end subroutine set_kinematics

  subroutine set_masses(setup)
    integer :: setup
    write(*,*) "Setting masses configuration", setup
    select case (setup)
      case (1)
        m02_qp = 0._qp
        m12_qp = 30._qp
        m22_qp = 10._qp
      case (2)
        m02_qp = 10._qp
        m12_qp = 10._qp
        m22_qp = 10._qp
      case (3)
        m02_qp = 0._qp
        m12_qp = 10._qp
        m22_qp = 0._qp
    end select

  end subroutine set_masses

  subroutine runcheck()

    p12_sp = p12_qp
    p23_sp = p23_qp
    p13_sp = p13_qp
    m02_sp = m02_qp
    m12_sp = m12_qp
    m22_sp = m22_qp

    p12_dp = p12_qp
    p23_dp = p23_qp
    p13_dp = p13_qp
    m02_dp = m02_qp
    m12_dp = m12_qp
    m22_dp = m22_qp

    p12_lp = p12_qp
    p23_lp = p23_qp
    p13_lp = p13_qp
    m02_lp = m02_qp
    m12_lp = m12_qp
    m22_lp = m22_qp

    call initcoli

    call setminf2(complex(10._sp, 0._sp))
    call setminf2(complex(10._dp, 0._dp))
    call setminf2(complex(10._lp, 0._lp))
    call setminf2(complex(10._qp, 0._qp))

    call setminf2(complex(30._sp, 0._sp))
    call setminf2(complex(30._dp, 0._dp))
    call setminf2(complex(30._lp, 0._lp))
    call setminf2(complex(30._qp, 0._qp))

    write(*,*) "Comparing sp vs dp"
    call compare(C0(p12_sp,p23_sp,p13_sp,m02_sp,m12_sp,m22_sp), &
                 C0(p12_dp,p23_dp,p13_dp,m02_dp,m12_dp,m22_dp), acc_coli_cmp-8)
    write(*,*) "Comparing dp vs lp"
    call compare(C0(p12_dp,p23_dp,p13_dp,m02_dp,m12_dp,m22_dp), &
                 C0(p12_lp,p23_lp,p13_lp,m02_lp,m12_lp,m22_lp), acc_coli_cmp)
    write(*,*) "Comparing dp vs qp"
    call compare(C0(p12_dp,p23_dp,p13_dp,m02_dp,m12_dp,m22_dp), &
                 C0(p12_qp,p23_qp,p13_qp,m02_qp,m12_qp,m22_qp), acc_coli_cmp)
    write(*,*) "Comparing lp vs qp"
    call compare(C0(p12_lp,p23_lp,p13_lp,m02_lp,m12_lp,m22_lp), &
                 C0(p12_qp,p23_qp,p13_qp,m02_qp,m12_qp,m22_qp), acc_coli_cmp)

    c0res_qp = C0(p12_qp,p23_qp,p13_qp,m02_qp,m12_qp,m22_qp)

    write(*,*) "C0 coliflower :"
    write(*,*) c0res_qp

    call setmuuv2(rescale**2)
    call setmuir2(rescale**2)

    call setminf2(complex(10._qp*rescale**2, 0._qp))
    call setminf2(complex(30._qp*rescale**2, 0._qp))

    p12_qp = p12_qp*rescale**2
    p23_qp = p23_qp*rescale**2
    p13_qp = p13_qp*rescale**2
    m02_qp = m02_qp*rescale**2
    m12_qp = m12_qp*rescale**2
    m22_qp = m22_qp*rescale**2
    acc_resc = C0(p12_qp,p23_qp,p13_qp,m02_qp,m12_qp,m22_qp)/c0res_qp*rescale**2
    write(*,*) "acc_resc:", acc_resc

    write(*,*) "Qp rescaling test"
    call compare(acc_resc, 1._qp, acc_resc_cmp)

#ifdef WITH_COLLIER
    call Init_cll(4,noreset=.true.)

    call SetMode_cll(1)
    call SetMuUV2_cll(1d0)
    call SetMuIR2_cll(1d0)
    allocate(minfcll(2))
    minfcll(1) = complex(10d0,0d0)
    minfcll(2) = complex(30d0,0d0)
    call SetMinf2_cll(2,minfcll)
    deallocate(minfcll)

    call C0_cll(c0res_dp,p12_dp,p23_dp,p13_dp,m02_dp,m12_dp,m22_dp)
    write(*,*) "C0 collier:"
    write(*,*) c0res_dp

    call compare(c0res_qp, c0res_dp, acc_coli_cmp)
#endif

  end subroutine runcheck

end program
