program C0test
  use coliflower, only: initcoli,C0,sp,dp,lp,qp,setmuir2,setmuuv2
  use params_coli_qp, only: pi2_6
  use test_tools, only: compare
#ifdef WITH_COLLIER
  use collier
#endif
#ifdef WITH_OLO
  use avh_olo, only: olo_scale_prec,olo
#endif
  implicit none

  complex(kind=qp) :: c0res_qp,c0res_qp_olo(0:2)
  complex(kind=dp) :: c0res_dp
  complex(kind=qp) :: p12_qp, p23_qp, p13_qp, m02_qp, m12_qp, m22_qp
  complex(kind=lp) :: p12_lp, p23_lp, p13_lp, m02_lp, m12_lp, m22_lp
  complex(kind=dp) :: p12_dp, p23_dp, p13_dp, m02_dp, m12_dp, m22_dp
  complex(kind=sp) :: p12_sp, p23_sp, p13_sp, m02_sp, m12_sp, m22_sp

  real(kind=qp) :: rescale = 1.3_qp,acc_resc

  integer :: acc_coli_cmp, acc_olo_cmp, acc_resc_cmp

  integer :: kconf, mconf

  do kconf = 1, 5
    do mconf = 1, 7
      call set_kinematics(kconf)
      call set_masses(mconf)
      call runcheck
    end do
  end do

  contains

  subroutine set_kinematics(setup)
    integer :: setup
    write(*,*) "Setting kinematics", setup
    select case (setup)
      case (1)
        p12_qp = complex(10000._qp,0._qp)
        p23_qp = complex(-10._qp,0._qp)
        p13_qp = complex(-10,0._qp)
        m02_qp = complex(0._qp,0._qp)
        m12_qp = complex(0._qp,0._qp)
        m22_qp = complex(0._qp,0._qp)
        acc_coli_cmp = 12
        acc_olo_cmp = 31
        acc_resc_cmp = 31
      case (2)
        p12_qp = complex(-10,0._qp)
        p23_qp = complex(10000._qp,0._qp)
        p13_qp = complex(-10,0._qp)
        m02_qp = complex(0._qp,0._qp)
        m12_qp = complex(100._qp,0._qp)
        m22_qp = complex(0._qp,0._qp)
        acc_coli_cmp = 12
        acc_olo_cmp = 31
        acc_resc_cmp = 31
      case (3)
        p12_qp = complex(0,0._qp)
        p23_qp = complex(100._qp,0._qp)
        p13_qp = complex(0,0._qp)
        m02_qp = complex(8._qp,0._qp)
        m12_qp = complex(100._qp,0._qp)
        m22_qp = complex(0._qp,0._qp)
        acc_coli_cmp = 11
        acc_olo_cmp = 29
        acc_resc_cmp = 29
      case (4)
        p12_qp = complex(0,0._qp)
        p23_qp = complex(100._qp,0._qp)
        p13_qp = complex(0,0._qp)
        m02_qp = complex(0._qp,0._qp)
        m12_qp = complex(0._qp,0._qp)
        m22_qp = complex(0._qp,0._qp)
        acc_coli_cmp = 11
        acc_olo_cmp = 29
        acc_resc_cmp = 29
      case (5)
        p12_qp = complex(0,0._qp)
        p23_qp = complex(0,0._qp)
        p13_qp = complex(55559.715559062999_qp,0._qp)
        m02_qp = complex(0._qp,0._qp)
        m12_qp = complex(0._qp,0._qp)
        m22_qp = complex(0._qp,0._qp)
        acc_coli_cmp = 14
        acc_olo_cmp = 30
        acc_resc_cmp = 30

    end select

  end subroutine set_kinematics

  subroutine set_masses(setup)
    integer :: setup
    write(*,*) "Setting masses configuration", setup
    select case (setup)
      case (1)
        m02_qp = 0._qp
        m12_qp = 0._qp
        m22_qp = 0._qp
      case (2)
        m02_qp = 300._qp
        m12_qp = 300._qp
        m22_qp = 300._qp
      case (3)
        m02_qp = 0._qp
        m12_qp = 300._qp
        m22_qp = 0._qp
      case (4)
        m02_qp = 300._qp
        m12_qp = 0._qp
        m22_qp = 0._qp
      case (5)
        m02_qp = 300._qp
        m12_qp = 0._qp
        m22_qp = 300._qp
      case (6)
        m02_qp = 0._qp
        m12_qp = 20._qp
        m22_qp = 300._qp
      case (7)
        m02_qp = 29584._qp
        m12_qp = 29584._qp
        m22_qp = 29584._qp
    end select

  end subroutine set_masses

  subroutine runcheck()

    p12_sp = p12_qp
    p23_sp = p23_qp
    p13_sp = p13_qp
    m02_sp = m02_qp
    m12_sp = m12_qp
    m22_sp = m22_qp

    p12_dp = p12_qp
    p23_dp = p23_qp
    p13_dp = p13_qp
    m02_dp = m02_qp
    m12_dp = m12_qp
    m22_dp = m22_qp

    p12_lp = p12_qp
    p23_lp = p23_qp
    p13_lp = p13_qp
    m02_lp = m02_qp
    m12_lp = m12_qp
    m22_lp = m22_qp

    call initcoli
    write(*,*) "Comparing sp vs dp"
    call compare(C0(p12_sp,p23_sp,p13_sp,m02_sp,m12_sp,m22_sp), &
                 C0(p12_dp,p23_dp,p13_dp,m02_dp,m12_dp,m22_dp), acc_coli_cmp-8)
    write(*,*) "Comparing dp vs lp"
    call compare(C0(p12_dp,p23_dp,p13_dp,m02_dp,m12_dp,m22_dp), &
                 C0(p12_lp,p23_lp,p13_lp,m02_lp,m12_lp,m22_lp), acc_coli_cmp)
    write(*,*) "Comparing dp vs qp"
    call compare(C0(p12_dp,p23_dp,p13_dp,m02_dp,m12_dp,m22_dp), &
                 C0(p12_qp,p23_qp,p13_qp,m02_qp,m12_qp,m22_qp), acc_coli_cmp)
    write(*,*) "Comparing lp vs qp"
    call compare(C0(p12_lp,p23_lp,p13_lp,m02_lp,m12_lp,m22_lp), &
                 C0(p12_qp,p23_qp,p13_qp,m02_qp,m12_qp,m22_qp), acc_coli_cmp)

    c0res_qp = C0(p12_qp,p23_qp,p13_qp,m02_qp,m12_qp,m22_qp)

    write(*,*) "C0 coliflower :"
    write(*,*) c0res_qp

#ifdef WITH_OLO
    call olo_scale_prec(1._qp)
    call olo(c0res_qp_olo,p12_qp,p23_qp,p13_qp,m02_qp,m12_qp,m22_qp)

    write(*,*) "C0 oneloop:"
    write(*,*) c0res_qp_olo(0)-c0res_qp_olo(2)*pi2_6

    call compare(c0res_qp, c0res_qp_olo(0)-c0res_qp_olo(2)*pi2_6, acc_olo_cmp)
#endif

    call setmuuv2(rescale**2)
    call setmuir2(rescale**2)

    p12_qp = p12_qp*rescale**2
    p23_qp = p23_qp*rescale**2
    p13_qp = p13_qp*rescale**2
    m02_qp = m02_qp*rescale**2
    m12_qp = m12_qp*rescale**2
    m22_qp = m22_qp*rescale**2
    acc_resc = C0(p12_qp,p23_qp,p13_qp,m02_qp,m12_qp,m22_qp)/c0res_qp*rescale**2
    write(*,*) "acc_resc:", acc_resc

    write(*,*) "Qp rescaling test"
    call compare(acc_resc, 1._qp, acc_resc_cmp)


#ifdef WITH_COLLIER
    call Init_cll(4,noreset=.true.)

    call SetMode_cll(1)
    call SetMuUV2_cll(1d0)
    call SetMuIR2_cll(1d0)

    call C0_cll(c0res_dp,p12_dp,p23_dp,p13_dp,m02_dp,m12_dp,m22_dp)
    write(*,*) "C0 collier:"
    write(*,*) c0res_dp

    call compare(c0res_qp, c0res_dp, acc_coli_cmp)
#endif

  end subroutine runcheck

end program
