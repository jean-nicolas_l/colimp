module test_tools
  use coliflower, only: sp, dp, lp, qp
  implicit none
  interface compare
    module procedure &
                     compare_sp_real_sp_real, &
                     compare_sp_real_dp_real, &
                     compare_sp_real_lp_real, &
                     compare_sp_real_qp_real, &
                     compare_dp_real_sp_real, &
                     compare_dp_real_dp_real, &
                     compare_dp_real_lp_real, &
                     compare_dp_real_qp_real, &
                     compare_lp_real_sp_real, &
                     compare_lp_real_dp_real, &
                     compare_lp_real_lp_real, &
                     compare_lp_real_qp_real, &
                     compare_qp_real_sp_real, &
                     compare_qp_real_dp_real, &
                     compare_qp_real_lp_real, &
                     compare_qp_real_qp_real, &
                     compare_sp_cmplx_sp_cmplx, &
                     compare_sp_cmplx_dp_cmplx, &
                     compare_sp_cmplx_lp_cmplx, &
                     compare_sp_cmplx_qp_cmplx, &
                     compare_dp_cmplx_sp_cmplx, &
                     compare_dp_cmplx_dp_cmplx, &
                     compare_dp_cmplx_lp_cmplx, &
                     compare_dp_cmplx_qp_cmplx, &
                     compare_lp_cmplx_sp_cmplx, &
                     compare_lp_cmplx_dp_cmplx, &
                     compare_lp_cmplx_lp_cmplx, &
                     compare_lp_cmplx_qp_cmplx, &
                     compare_qp_cmplx_sp_cmplx, &
                     compare_qp_cmplx_dp_cmplx, &
                     compare_qp_cmplx_lp_cmplx, &
                     compare_qp_cmplx_qp_cmplx
  end interface compare

  contains

  subroutine compare_sp_real_sp_real(num_a, num_b, target_accuracy)
    real(kind=sp), intent(in) :: num_a
    real(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_real_sp_real


  subroutine compare_dp_real_sp_real(num_a, num_b, target_accuracy)
    real(kind=dp), intent(in) :: num_a
    real(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_real_sp_real


  subroutine compare_lp_real_sp_real(num_a, num_b, target_accuracy)
    real(kind=lp), intent(in) :: num_a
    real(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_real_sp_real


  subroutine compare_qp_real_sp_real(num_a, num_b, target_accuracy)
    real(kind=qp), intent(in) :: num_a
    real(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_real_sp_real


  subroutine compare_sp_cmplx_sp_real(num_a, num_b, target_accuracy)
    complex(kind=sp), intent(in) :: num_a
    real(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_cmplx_sp_real


  subroutine compare_dp_cmplx_sp_real(num_a, num_b, target_accuracy)
    complex(kind=dp), intent(in) :: num_a
    real(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_cmplx_sp_real


  subroutine compare_lp_cmplx_sp_real(num_a, num_b, target_accuracy)
    complex(kind=lp), intent(in) :: num_a
    real(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_cmplx_sp_real


  subroutine compare_qp_cmplx_sp_real(num_a, num_b, target_accuracy)
    complex(kind=qp), intent(in) :: num_a
    real(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_cmplx_sp_real


  subroutine compare_sp_real_dp_real(num_a, num_b, target_accuracy)
    real(kind=sp), intent(in) :: num_a
    real(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_real_dp_real


  subroutine compare_dp_real_dp_real(num_a, num_b, target_accuracy)
    real(kind=dp), intent(in) :: num_a
    real(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_real_dp_real


  subroutine compare_lp_real_dp_real(num_a, num_b, target_accuracy)
    real(kind=lp), intent(in) :: num_a
    real(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_real_dp_real


  subroutine compare_qp_real_dp_real(num_a, num_b, target_accuracy)
    real(kind=qp), intent(in) :: num_a
    real(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_real_dp_real


  subroutine compare_sp_cmplx_dp_real(num_a, num_b, target_accuracy)
    complex(kind=sp), intent(in) :: num_a
    real(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_cmplx_dp_real


  subroutine compare_dp_cmplx_dp_real(num_a, num_b, target_accuracy)
    complex(kind=dp), intent(in) :: num_a
    real(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_cmplx_dp_real


  subroutine compare_lp_cmplx_dp_real(num_a, num_b, target_accuracy)
    complex(kind=lp), intent(in) :: num_a
    real(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_cmplx_dp_real


  subroutine compare_qp_cmplx_dp_real(num_a, num_b, target_accuracy)
    complex(kind=qp), intent(in) :: num_a
    real(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_cmplx_dp_real


  subroutine compare_sp_real_lp_real(num_a, num_b, target_accuracy)
    real(kind=sp), intent(in) :: num_a
    real(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_real_lp_real


  subroutine compare_dp_real_lp_real(num_a, num_b, target_accuracy)
    real(kind=dp), intent(in) :: num_a
    real(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_real_lp_real


  subroutine compare_lp_real_lp_real(num_a, num_b, target_accuracy)
    real(kind=lp), intent(in) :: num_a
    real(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_real_lp_real


  subroutine compare_qp_real_lp_real(num_a, num_b, target_accuracy)
    real(kind=qp), intent(in) :: num_a
    real(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_real_lp_real


  subroutine compare_sp_cmplx_lp_real(num_a, num_b, target_accuracy)
    complex(kind=sp), intent(in) :: num_a
    real(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_cmplx_lp_real


  subroutine compare_dp_cmplx_lp_real(num_a, num_b, target_accuracy)
    complex(kind=dp), intent(in) :: num_a
    real(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_cmplx_lp_real


  subroutine compare_lp_cmplx_lp_real(num_a, num_b, target_accuracy)
    complex(kind=lp), intent(in) :: num_a
    real(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_cmplx_lp_real


  subroutine compare_qp_cmplx_lp_real(num_a, num_b, target_accuracy)
    complex(kind=qp), intent(in) :: num_a
    real(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_cmplx_lp_real


  subroutine compare_sp_real_qp_real(num_a, num_b, target_accuracy)
    real(kind=sp), intent(in) :: num_a
    real(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_real_qp_real


  subroutine compare_dp_real_qp_real(num_a, num_b, target_accuracy)
    real(kind=dp), intent(in) :: num_a
    real(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_real_qp_real


  subroutine compare_lp_real_qp_real(num_a, num_b, target_accuracy)
    real(kind=lp), intent(in) :: num_a
    real(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_real_qp_real


  subroutine compare_qp_real_qp_real(num_a, num_b, target_accuracy)
    real(kind=qp), intent(in) :: num_a
    real(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_real_qp_real


  subroutine compare_sp_cmplx_qp_real(num_a, num_b, target_accuracy)
    complex(kind=sp), intent(in) :: num_a
    real(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_cmplx_qp_real


  subroutine compare_dp_cmplx_qp_real(num_a, num_b, target_accuracy)
    complex(kind=dp), intent(in) :: num_a
    real(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_cmplx_qp_real


  subroutine compare_lp_cmplx_qp_real(num_a, num_b, target_accuracy)
    complex(kind=lp), intent(in) :: num_a
    real(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_cmplx_qp_real


  subroutine compare_qp_cmplx_qp_real(num_a, num_b, target_accuracy)
    complex(kind=qp), intent(in) :: num_a
    real(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_cmplx_qp_real


  subroutine compare_sp_real_sp_cmplx(num_a, num_b, target_accuracy)
    real(kind=sp), intent(in) :: num_a
    complex(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_real_sp_cmplx


  subroutine compare_dp_real_sp_cmplx(num_a, num_b, target_accuracy)
    real(kind=dp), intent(in) :: num_a
    complex(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_real_sp_cmplx


  subroutine compare_lp_real_sp_cmplx(num_a, num_b, target_accuracy)
    real(kind=lp), intent(in) :: num_a
    complex(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_real_sp_cmplx


  subroutine compare_qp_real_sp_cmplx(num_a, num_b, target_accuracy)
    real(kind=qp), intent(in) :: num_a
    complex(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_real_sp_cmplx


  subroutine compare_sp_cmplx_sp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=sp), intent(in) :: num_a
    complex(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_cmplx_sp_cmplx


  subroutine compare_dp_cmplx_sp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=dp), intent(in) :: num_a
    complex(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_cmplx_sp_cmplx


  subroutine compare_lp_cmplx_sp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=lp), intent(in) :: num_a
    complex(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_cmplx_sp_cmplx


  subroutine compare_qp_cmplx_sp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=qp), intent(in) :: num_a
    complex(kind=sp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_cmplx_sp_cmplx


  subroutine compare_sp_real_dp_cmplx(num_a, num_b, target_accuracy)
    real(kind=sp), intent(in) :: num_a
    complex(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_real_dp_cmplx


  subroutine compare_dp_real_dp_cmplx(num_a, num_b, target_accuracy)
    real(kind=dp), intent(in) :: num_a
    complex(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_real_dp_cmplx


  subroutine compare_lp_real_dp_cmplx(num_a, num_b, target_accuracy)
    real(kind=lp), intent(in) :: num_a
    complex(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_real_dp_cmplx


  subroutine compare_qp_real_dp_cmplx(num_a, num_b, target_accuracy)
    real(kind=qp), intent(in) :: num_a
    complex(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_real_dp_cmplx


  subroutine compare_sp_cmplx_dp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=sp), intent(in) :: num_a
    complex(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_cmplx_dp_cmplx


  subroutine compare_dp_cmplx_dp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=dp), intent(in) :: num_a
    complex(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_cmplx_dp_cmplx


  subroutine compare_lp_cmplx_dp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=lp), intent(in) :: num_a
    complex(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_cmplx_dp_cmplx


  subroutine compare_qp_cmplx_dp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=qp), intent(in) :: num_a
    complex(kind=dp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_cmplx_dp_cmplx


  subroutine compare_sp_real_lp_cmplx(num_a, num_b, target_accuracy)
    real(kind=sp), intent(in) :: num_a
    complex(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_real_lp_cmplx


  subroutine compare_dp_real_lp_cmplx(num_a, num_b, target_accuracy)
    real(kind=dp), intent(in) :: num_a
    complex(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_real_lp_cmplx


  subroutine compare_lp_real_lp_cmplx(num_a, num_b, target_accuracy)
    real(kind=lp), intent(in) :: num_a
    complex(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_real_lp_cmplx


  subroutine compare_qp_real_lp_cmplx(num_a, num_b, target_accuracy)
    real(kind=qp), intent(in) :: num_a
    complex(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_real_lp_cmplx


  subroutine compare_sp_cmplx_lp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=sp), intent(in) :: num_a
    complex(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_cmplx_lp_cmplx


  subroutine compare_dp_cmplx_lp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=dp), intent(in) :: num_a
    complex(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_cmplx_lp_cmplx


  subroutine compare_lp_cmplx_lp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=lp), intent(in) :: num_a
    complex(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_cmplx_lp_cmplx


  subroutine compare_qp_cmplx_lp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=qp), intent(in) :: num_a
    complex(kind=lp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_cmplx_lp_cmplx


  subroutine compare_sp_real_qp_cmplx(num_a, num_b, target_accuracy)
    real(kind=sp), intent(in) :: num_a
    complex(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_real_qp_cmplx


  subroutine compare_dp_real_qp_cmplx(num_a, num_b, target_accuracy)
    real(kind=dp), intent(in) :: num_a
    complex(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_real_qp_cmplx


  subroutine compare_lp_real_qp_cmplx(num_a, num_b, target_accuracy)
    real(kind=lp), intent(in) :: num_a
    complex(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_real_qp_cmplx


  subroutine compare_qp_real_qp_cmplx(num_a, num_b, target_accuracy)
    real(kind=qp), intent(in) :: num_a
    complex(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_real_qp_cmplx


  subroutine compare_sp_cmplx_qp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=sp), intent(in) :: num_a
    complex(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_sp_cmplx_qp_cmplx


  subroutine compare_dp_cmplx_qp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=dp), intent(in) :: num_a
    complex(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_dp_cmplx_qp_cmplx


  subroutine compare_lp_cmplx_qp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=lp), intent(in) :: num_a
    complex(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_lp_cmplx_qp_cmplx


  subroutine compare_qp_cmplx_qp_cmplx(num_a, num_b, target_accuracy)
    complex(kind=qp), intent(in) :: num_a
    complex(kind=qp), intent(in) :: num_b
    integer, intent(in) :: target_accuracy
    real*8 :: acc

    acc = -log(abs(abs(num_a/num_b)-1))/log(10.)

    if (acc < target_accuracy) then
      write(*,*) "Comparing:"
      write(*,*)  num_a
      write(*,*)  num_b
      write(*,*) "Accuracy target of ", target_accuracy, "  digits not reached."
      write(*,*) "Obtained           ", int(acc), "  digits instead."
      stop 9
    end if

  end subroutine compare_qp_cmplx_qp_cmplx

end module
