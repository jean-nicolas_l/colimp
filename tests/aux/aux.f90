program aux
  use coliflower, only: dp, lp, qp
  use coli_aux_qp, only: cspenc_coli
  use test_tools, only: compare
  implicit none

  complex(qp) :: check,arg

  !write(*,*) "cspenc_coli(arg_dp):", cspenc_coli(cmplx(1._qp,-7._qp,kind=qp),1._qp)
  !arg_dp = cmplx(1d0, -7d0)
  !write(*,*) "cspenc_coli(arg_dp):", cspenc_coli(arg_dp,1d0)

  check = cmplx(-2.10635786868637740561545711938741051_qp, &
                -3.49115016154644037353853237626027671_qp,kind=qp)
  call compare(cspenc_coli(cmplx(1._qp,-7._qp,kind=qp),1._qp), check, 33)

  check = cmplx(-20.5986906620374944169259910455479572_qp, &
                -21.6562647088039533259252310269405565_qp,kind=qp)
  call compare(cspenc_coli(cmplx(1001._qp,-7._qp,kind=qp),1._qp), check, 33)


  arg = cmplx(0.482298034318441292742304770279799945_qp, &
                0.846580752435627721146311903310779_qp, kind=qp)
  check = cmplx(0.268099461770660121237177710927032_qp, &
                0.988055771319620969319051879457352_qp, kind=qp)

  call compare(cspenc_coli(arg, 1._qp), check, 33)
  
end program
